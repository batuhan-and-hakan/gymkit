package com.gymkit.gymkit.unsignedoperations;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity {

    EditText editText, editText2, editText3, editText4, editText5, editText6, editText7;
    RadioButton maleRadioButton, femaleRadioButton, yesRadioButton, noRadioButton;
    Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        getSupportActionBar().setTitle("Sign Up");

        editText = findViewById(R.id.editText);
        editText2 = findViewById(R.id.editText2);
        editText3 = findViewById(R.id.editText3);
        editText4 = findViewById(R.id.editText4);
        editText5 = findViewById(R.id.editText5);
        editText6 = findViewById(R.id.editText6);
        editText7 = findViewById(R.id.editText7);

        maleRadioButton = findViewById(R.id.maleRadioButton);
        femaleRadioButton = findViewById(R.id.femaleRadioButton);
        yesRadioButton = findViewById(R.id.yesRadioButton);
        noRadioButton = findViewById(R.id.noRadioButton);

        saveButton = findViewById(R.id.saveButton);
    }

    public void save(View view)
    {

        if (!maleRadioButton.isChecked() && !femaleRadioButton.isChecked())
            Toast.makeText(this, "All blanks must be filled!", Toast.LENGTH_LONG).show();

        else if (yesRadioButton.isChecked() == false && noRadioButton.isChecked() == false)
            Toast.makeText(this, "All blanks must be filled!", Toast.LENGTH_LONG).show();

        else if (yesRadioButton.isChecked() && (editText.getText().toString().equals("") || editText2.getText().toString().equals("") || editText3.getText().toString().equals("") || editText4.getText().toString().equals("") || editText5.getText().toString().equals("") || editText6.getText().toString().equals("") || editText7.getText().toString().equals("")))
            Toast.makeText(this, "All blanks must be filled!", Toast.LENGTH_LONG).show();

        else if (noRadioButton.isChecked() && (editText.getText().toString().equals("") || editText2.getText().toString().equals("") || editText3.getText().toString().equals("") || editText4.getText().toString().equals("") || editText5.getText().toString().equals("") || editText6.getText().toString().equals("")))
            Toast.makeText(this, "All blanks must be filled!", Toast.LENGTH_LONG).show();

        else if (!editText3.getText().toString().contains("@"))
            Toast.makeText(this, "Wrong email format!", Toast.LENGTH_LONG).show();

        else
        {
            saveButton.setEnabled(false);

            String url = "http://batuhanbatu.net/gymkitwebservice/insert_user.php";

            StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    if (response.contains("Successfully added"))
                    {

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SignUpActivity.this);
                        alertDialogBuilder.setTitle("Signed Up Successfully");
                        alertDialogBuilder.setCancelable(false);
                        alertDialogBuilder.setMessage("Your profile was created successfully! Now, you can go to login page");

                        alertDialogBuilder.setPositiveButton("Go To Login", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    }

                    else if (response.contains("Already registered"))
                    {
                        Toast.makeText(SignUpActivity.this, "This email is already registered to this app!", Toast.LENGTH_LONG).show();
                        saveButton.setEnabled(true);
                    }

                    else if (response.contains("Wrong code"))
                    {
                        Toast.makeText(SignUpActivity.this, "Your code is wrong!", Toast.LENGTH_LONG).show();
                        saveButton.setEnabled(true);
                    }

                    else
                    {
                        Toast.makeText(SignUpActivity.this, "Error!", Toast.LENGTH_LONG).show();
                        saveButton.setEnabled(true);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(SignUpActivity.this, "Errorr!", Toast.LENGTH_LONG).show();
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();

                    params.put("fname", editText.getText().toString());
                    params.put("lname", editText2.getText().toString());
                    params.put("email", editText3.getText().toString());
                    params.put("password", editText4.getText().toString());
                    params.put("phone", editText5.getText().toString());

                    if (maleRadioButton.isChecked())
                        params.put("gender", "1");
                    if (femaleRadioButton.isChecked())
                        params.put("gender", "0");

                    params.put("bday", editText6.getText().toString());

                    if (yesRadioButton.isChecked())
                        params.put("code", editText7.getText().toString());
                    if (noRadioButton.isChecked())
                        params.put("code", "0");

                    return params;
                }
            };

            Volley.newRequestQueue(this).add(request);

        }
    }

    public void activateGymCodeEditText(View view)
    {
        editText7.setEnabled(true);
    }

    public void disactivateGymCodeEditText(View view)
    {
        editText7.setText("");
        editText7.setEnabled(false);
    }

    public void pickDate(View view)
    {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month += 1;
                        editText6.setText(year + "-" + month + "-" + dayOfMonth);
                    }
                }, year, month, dayOfMonth);

        dpd.setButton(DatePickerDialog.BUTTON_POSITIVE, "SELECT", dpd);
        dpd.setButton(DatePickerDialog.BUTTON_NEGATIVE, "CANCEL", dpd);
        dpd.show();
    }
}
