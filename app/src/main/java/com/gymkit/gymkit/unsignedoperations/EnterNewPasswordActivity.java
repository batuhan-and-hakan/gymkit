package com.gymkit.gymkit.unsignedoperations;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;

import java.util.HashMap;
import java.util.Map;

public class EnterNewPasswordActivity extends AppCompatActivity {

    String email;
    EditText editText, editText2;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_new_password);

        getSupportActionBar().setTitle("Enter New Password");

        editText = findViewById(R.id.editText);
        editText2 = findViewById(R.id.editText2);
        button = findViewById(R.id.button);
        Intent getintent = getIntent();
        email = getintent.getStringExtra("email");
    }

    public void change (View view) {
        if (editText.getText().toString().equals("") || editText2.getText().toString().equals("") || !editText.getText().toString().equals(editText2.getText().toString()))
            Toast.makeText(this, "Passwords must be equal and can not be empty!", Toast.LENGTH_LONG).show();
        else {

            button.setEnabled(false);

            final String newPassword = editText.getText().toString();

            String url = "http://batuhanbatu.net/gymkitwebservice/update_password.php";

            StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(EnterNewPasswordActivity.this);
                    alertDialogBuilder.setTitle("Password Changed");
                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.setMessage("Your password was changed successfully! Now, you can go to login page");

                    alertDialogBuilder.setPositiveButton("Go To Login", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(EnterNewPasswordActivity.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();

                    params.put("email", email);
                    params.put("password", newPassword);

                    return params;
                }
            };

            Volley.newRequestQueue(this).add(request);

        }
    }
}
