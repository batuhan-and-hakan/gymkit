package com.gymkit.gymkit.unsignedoperations;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    //#3584AA -> Color in the logo

    EditText editText, editText2;
    Button button, button2;

    String enteredMail, enteredPassword;

    SharedPreferences session;
    SharedPreferences.Editor sessionEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        sessionEditor = session.edit();

        getSupportActionBar().setTitle("Login");

        if (!session.getString("fname", "").equals("")) {

            Intent intent = new Intent(MainActivity.this, ArticleListActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }

        editText = findViewById(R.id.editText);
        editText2 = findViewById(R.id.editText2);
        button = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);
    }

    public void login(View view) {

        if (editText.getText().toString().equals("") || editText2.getText().toString().equals("")) {
            Toast.makeText(this, "Both fields must not be empty!", Toast.LENGTH_LONG).show();
        }
        else if (!editText.getText().toString().contains("@")) {
            Toast.makeText(this, "Wrong email format!", Toast.LENGTH_LONG).show();
        }
        else {

            button.setEnabled(false);
            button2.setEnabled(false);

            enteredMail = editText.getText().toString();
            enteredPassword = editText2.getText().toString();

            String url = "http://batuhanbatu.net/gymkitwebservice/login.php";

            StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try
                    {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray result = jsonObject.getJSONArray("user");

                        for (int i = 0; i < result.length(); i++)
                        {
                            JSONObject row = result.getJSONObject(i);

                            sessionEditor.putInt("id", row.getInt("id"));
                            sessionEditor.putString("fname", row.getString("fname"));
                            sessionEditor.putString("lname", row.getString("lname"));
                            sessionEditor.putString("email", row.getString("email"));
                            sessionEditor.putString("password", row.getString("password"));
                            sessionEditor.putString("phone", row.getString("phone"));
                            sessionEditor.putString("gender", row.getString("gender"));
                            sessionEditor.putString("bday", row.getString("bday"));
                            sessionEditor.putInt("gymid", row.getInt("gymid"));
                            sessionEditor.putString("usertype", row.getString("usertype"));
                            sessionEditor.putString("activation", row.getString("activation"));
                            sessionEditor.apply();
                            Intent intent = new Intent(MainActivity.this, ArticleListActivity.class);
                            startActivity(intent);
                            finish();

                        }

                    }

                    catch (JSONException e)
                    {
                        e.printStackTrace();
                        button.setEnabled(true);
                        button2.setEnabled(true);
                        Toast.makeText(MainActivity.this, "Your login information(s) is wrong!", Toast.LENGTH_LONG).show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();

                    params.put("email", enteredMail);
                    params.put("password", enteredPassword);

                    return params;
                }
            };

            Volley.newRequestQueue(this).add(request);

        }
    }

    public void signup(View view) {

        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);

    }

    public void forgotpassword(View view) {

        Intent intent = new Intent(this, ForgotPasswordActivity.class);
        startActivity(intent);

    }
}