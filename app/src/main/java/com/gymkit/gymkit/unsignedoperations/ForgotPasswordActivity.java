package com.gymkit.gymkit.unsignedoperations;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class ForgotPasswordActivity extends AppCompatActivity {

    EditText editText;
    String email, fullname;
    int code;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        editText = findViewById(R.id.editText);
        button = findViewById(R.id.button);
        getSupportActionBar().setTitle("Forgot Password");
    }

    public void sendTheCode(View view) {

        email = editText.getText().toString();

        if (email.equals("") || !email.contains("@")) {
            Toast.makeText(this, "Wrong email format! Try again", Toast.LENGTH_LONG).show();
        }

        else {

            button.setEnabled(false);

            String url = "http://batuhanbatu.net/gymkitwebservice/get_user_by_email.php";

            StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try
                    {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray result = jsonObject.getJSONArray("user");

                        for (int i = 0; i < result.length(); i++)
                        {
                            JSONObject row = result.getJSONObject(i);

                            fullname = row.getString("fname") + " " + row.getString("lname");

                            Random rand = new Random();
                            code = rand.nextInt(900000) + 100000;

                            try {
                                GMailSender sender = new GMailSender("gymkitapp@gmail.com","batkan1961");
                                sender.sendMail(
                                        "Your Password Reset Code on GymKit",
                                        "Hello "+ fullname + ",\n\nYour code is " + Integer.toString(code) + "\n\nPlease write this code to the related area in the application\n\nHave a good day!\n\nGymKit",
                                        "gymkitapp@gmail.com", email);
                            } catch (Exception e) {
                                Toast.makeText(ForgotPasswordActivity.this, e.getLocalizedMessage().toString(), Toast.LENGTH_LONG).show();
                            }

                            Intent intent = new Intent(ForgotPasswordActivity.this, EnterTheCodeActivity.class);
                            intent.putExtra("code", Integer.toString(code));
                            intent.putExtra("email", email);
                            startActivity(intent);
                            finish();

                        }

                    }

                    catch (JSONException e)
                    {
                        e.printStackTrace();
                        button.setEnabled(true);
                        Toast.makeText(ForgotPasswordActivity.this, "This email has not registered to this app before!", Toast.LENGTH_LONG).show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();

                    params.put("email", email);

                    return params;
                }
            };

            Volley.newRequestQueue(this).add(request);

        }

    }
}