package com.gymkit.gymkit.signedoperations.bodysize;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class UpdateBodySizeActivity extends AppCompatActivity {

    SharedPreferences session;
    String id = "";
    String userid = "";
    EditText name, weight, fluid, oil, muscle;
    Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_body_size);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Update Body Size");

        Intent getintent = getIntent();
        id = getintent.getStringExtra("id");
        userid = getintent.getStringExtra("userid");

        name = findViewById(R.id.name);
        weight = findViewById(R.id.weight);
        fluid = findViewById(R.id.fluid);
        oil = findViewById(R.id.oil);
        muscle = findViewById(R.id.muscle);
        saveButton = findViewById(R.id.saveButton);

        fillTheBlanks();
    }

    public void fillTheBlanks()
    {
        String url = "http://batuhanbatu.net/gymkitwebservice/get_body_size_analysis.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("body_size");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        name.setText(row.getString("name"));
                        weight.setText(row.getString("weight"));
                        fluid.setText(row.getString("fluid"));
                        oil.setText(row.getString("oil"));
                        muscle.setText(row.getString("muscle"));
                    }

                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("id", id);

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }

    public void save(View view)
    {
        if (name.getText().toString().equals("") || weight.getText().toString().equals("") || fluid.getText().toString().equals("") || oil.getText().toString().equals("") || muscle.getText().toString().equals(""))
            Toast.makeText(this, "All Fields Must Not Be Empty!", Toast.LENGTH_SHORT).show();

        else if (Double.parseDouble(weight.getText().toString()) < 0 || Double.parseDouble(weight.getText().toString()) > 250 || Double.parseDouble(fluid.getText().toString()) < 0 || Double.parseDouble(fluid.getText().toString()) > 100 || Double.parseDouble(oil.getText().toString()) < 0 || Double.parseDouble(oil.getText().toString()) > 100 || Double.parseDouble(muscle.getText().toString()) < 0 || Double.parseDouble(muscle.getText().toString()) > 100)
            Toast.makeText(this, "Fields Must Be Filled By Valid Numbers!", Toast.LENGTH_SHORT).show();

        else
        {
            saveButton.setEnabled(false);

            String url = "http://batuhanbatu.net/gymkitwebservice/update_body_size.php";

            StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    if (response.contains("Successfully updated"))
                    {

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(UpdateBodySizeActivity.this);
                        alertDialogBuilder.setTitle("Updated Successfully");
                        alertDialogBuilder.setCancelable(false);
                        alertDialogBuilder.setMessage("The body size was updated successfully!");

                        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent intent = new Intent(UpdateBodySizeActivity.this, UsersBodySizeListActivity.class);
                                intent.putExtra("userid", userid);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    }

                    else
                    {
                        Toast.makeText(UpdateBodySizeActivity.this, "Error!", Toast.LENGTH_LONG).show();
                        saveButton.setEnabled(true);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    saveButton.setEnabled(true);
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();

                    params.put("id", id);
                    params.put("name", name.getText().toString().replace("'", "~"));
                    params.put("weight", weight.getText().toString());
                    params.put("fluid", fluid.getText().toString());
                    params.put("oil", oil.getText().toString());
                    params.put("muscle", muscle.getText().toString());

                    return params;
                }
            };

            Volley.newRequestQueue(this).add(request);
        }


    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }
}