package com.gymkit.gymkit.signedoperations.profile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.article.ArticleListRecyclerAdapter;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SavedArticleActivity extends AppCompatActivity {

    SharedPreferences session;
    ArticleListRecyclerAdapter articleListRecyclerAdapter;
    ArrayList<String> idListFromDB;
    ArrayList<String> titleListFromDB;
    ArrayList<String> photoListFromDB;
    ArrayList<String> contentListFromDB;
    ArrayList<String> dateListFromDB;

    Date currentDate;
    long currentTimeMilli;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_article);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Saved Articles");

        idListFromDB = new ArrayList<>();
        titleListFromDB = new ArrayList<>();
        photoListFromDB = new ArrayList<>();
        contentListFromDB = new ArrayList<>();
        dateListFromDB = new ArrayList<>();

        getArticles();

        currentDate = new Date();
        currentTimeMilli = currentDate.getTime();
        context = SavedArticleActivity.this;

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        articleListRecyclerAdapter = new ArticleListRecyclerAdapter(this, currentTimeMilli, idListFromDB, titleListFromDB, photoListFromDB, contentListFromDB, dateListFromDB);
        recyclerView.setAdapter(articleListRecyclerAdapter);

        final SwipeRefreshLayout pullToRefresh = findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentDate = new Date();
                currentTimeMilli = currentDate.getTime();

                RecyclerView recyclerView = findViewById(R.id.recyclerView);
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
                articleListRecyclerAdapter = new ArticleListRecyclerAdapter(context, currentTimeMilli, idListFromDB, titleListFromDB, photoListFromDB, contentListFromDB, dateListFromDB);
                recyclerView.setAdapter(articleListRecyclerAdapter);// your code
                pullToRefresh.setRefreshing(false);
            }
        });
    }

    public void getArticles()
    {
        idListFromDB.removeAll(idListFromDB);
        titleListFromDB.removeAll(titleListFromDB);
        photoListFromDB.removeAll(photoListFromDB);
        contentListFromDB.removeAll(contentListFromDB);
        dateListFromDB.removeAll(dateListFromDB);

        String url = "http://batuhanbatu.net/gymkitwebservice/get_saved_articles.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("article");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        idListFromDB.add(String.valueOf(row.getInt("id")));
                        titleListFromDB.add(row.getString("title").replace("~", "'"));
                        photoListFromDB.add(row.getString("photo"));
                        contentListFromDB.add(row.getString("content").replace("~", "'"));
                        dateListFromDB.add(row.getString("date"));

                    }

                    currentDate = new Date();
                    currentTimeMilli = currentDate.getTime();

                    articleListRecyclerAdapter.notifyDataSetChanged();
                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("gymid", String.valueOf(session.getInt("gymid", 0)));
                params.put("userid", String.valueOf(session.getInt("id", 0)));

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);

    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(SavedArticleActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(SavedArticleActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(SavedArticleActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(SavedArticleActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(SavedArticleActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}