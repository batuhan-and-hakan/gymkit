package com.gymkit.gymkit.signedoperations.article;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.method.ScrollingMovementMethod;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Scroller;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class UpdateArticleActivity extends AppCompatActivity {

    SharedPreferences session;
    int articleid;
    EditText editText, editText2;
    ImageView imageView;
    Button chooseaphotobutton, saveButton;
    Bitmap bitmap;
    final int IMG_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_article);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Update Article");

        Intent getintent = getIntent();
        articleid = Integer.valueOf(getintent.getStringExtra("articleid"));

        editText = findViewById(R.id.editText);
        editText2 = findViewById(R.id.editText2);
        imageView = findViewById(R.id.imageView);
        chooseaphotobutton = findViewById(R.id.chooseaphotobutton);
        saveButton = findViewById(R.id.saveButton);

        editText2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.editText2) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        fillTheViews();
    }

    public void fillTheViews()
    {

        String url = "http://batuhanbatu.net/gymkitwebservice/get_article_by_id.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("article");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        editText.setText(row.getString("title").replace("~", "'"));
                        editText2.setText(row.getString("content").replace("~", "'"));

                        Picasso.get().load(row.getString("photo")).into(imageView);

                    }
                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("id", String.valueOf(articleid));

                return params;
            }
        };

        Volley.newRequestQueue(UpdateArticleActivity.this).add(request);

    }

    public void choosePhoto(View view)
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, IMG_REQUEST_CODE);
    }

    public String imageToString(Bitmap bitmap)
    {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] imgBytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgBytes, Base64.DEFAULT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMG_REQUEST_CODE && resultCode == RESULT_OK && data != null)
        {
            Uri path = data.getData();

            if (Build.VERSION.SDK_INT >= 29) {
                ImageDecoder.Source source = ImageDecoder.createSource(getApplicationContext().getContentResolver(), path);
                try {
                    bitmap = ImageDecoder.decodeBitmap(source);
                    imageView.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {

                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), path);
                    imageView.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public void save(View view)
    {
        saveButton.setEnabled(false);

        if (editText.getText().toString().equals("") || editText2.getText().toString().equals(""))
        {
            Toast.makeText(this, "All blanks must be filled!", Toast.LENGTH_LONG).show();
            saveButton.setEnabled(true);
        }

        else
        {

            if (bitmap == null)
            {
                String url = "http://batuhanbatu.net/gymkitwebservice/update_article_without_photo.php";

                StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response.contains("Successfully updated"))
                        {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(UpdateArticleActivity.this);
                            alertDialogBuilder.setTitle("Your article was updated successfully!");
                            alertDialogBuilder.setCancelable(false);
                            alertDialogBuilder.setMessage("Now, you can see the article..");

                            alertDialogBuilder.setPositiveButton("Go To The Article", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    Intent intent = new Intent(UpdateArticleActivity.this, ArticleListActivity.class);
                                    intent.putExtra("articleid", String.valueOf(articleid));
                                    startActivity(intent);
                                    finish();
                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }

                        else
                        {
                            Toast.makeText(UpdateArticleActivity.this, "Error!", Toast.LENGTH_LONG).show();
                            saveButton.setEnabled(true);
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(UpdateArticleActivity.this, "Errorr!", Toast.LENGTH_LONG).show();
                        saveButton.setEnabled(true);
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<>();

                        params.put("id", String.valueOf(articleid));
                        params.put("title", editText.getText().toString().replace("'", "~"));
                        params.put("content", editText2.getText().toString().replace("'", "~"));

                        return params;
                    }
                };

                Volley.newRequestQueue(this).add(request);
            }

            else
            {
                String url = "http://batuhanbatu.net/gymkitwebservice/update_article_with_photo.php";

                StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response.contains("Successfully updated"))
                        {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(UpdateArticleActivity.this);
                            alertDialogBuilder.setTitle("Your article was updated successfully!");
                            alertDialogBuilder.setCancelable(false);
                            alertDialogBuilder.setMessage("Now, you can see the article..");

                            alertDialogBuilder.setPositiveButton("Go To The Article", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    Intent intent = new Intent(UpdateArticleActivity.this, ArticleDetailActivity.class);
                                    intent.putExtra("articleid", String.valueOf(articleid));
                                    startActivity(intent);
                                    finish();
                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }

                        else
                        {
                            Toast.makeText(UpdateArticleActivity.this, "Error!", Toast.LENGTH_LONG).show();
                            saveButton.setEnabled(true);
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(UpdateArticleActivity.this, "Errorr!", Toast.LENGTH_LONG).show();
                        saveButton.setEnabled(true);
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<>();

                        params.put("id", String.valueOf(articleid));
                        params.put("title", editText.getText().toString().replace("'", "~"));
                        params.put("photo", imageToString(bitmap));
                        params.put("content", editText2.getText().toString().replace("'", "~"));

                        Date date = new Date();
                        long timeMilli = date.getTime();
                        params.put("photoname", String.valueOf(timeMilli));

                        return params;
                    }
                };

                Volley.newRequestQueue(this).add(request);
            }

        }
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(UpdateArticleActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(UpdateArticleActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(UpdateArticleActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(UpdateArticleActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(UpdateArticleActivity.this, ProfileActivity.class);
        startActivity(intent);
    }

}
