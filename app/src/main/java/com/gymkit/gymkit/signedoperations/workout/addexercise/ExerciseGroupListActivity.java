package com.gymkit.gymkit.signedoperations.workout.addexercise;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.UserListToWorkoutActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ExerciseGroupListActivity extends AppCompatActivity {

    SharedPreferences session;
    ExerciseGroupListRecyclerAdapter exerciseGroupListRecyclerAdapter;
    ArrayList<String> idListFromDB;
    ArrayList<String> nameListFromDB;
    ArrayList<String> photoListFromDB;

    String workoutid = "";
    String creatorid = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_group_list);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Exercise Group List");

        Intent getintent = getIntent();
        workoutid = getintent.getStringExtra("workoutid");
        creatorid = getintent.getStringExtra("creatorid");

        idListFromDB = new ArrayList<>();
        nameListFromDB = new ArrayList<>();
        photoListFromDB = new ArrayList<>();

        getExerciseGroupList();

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        exerciseGroupListRecyclerAdapter = new ExerciseGroupListRecyclerAdapter(this, workoutid, creatorid, idListFromDB, nameListFromDB, photoListFromDB);
        recyclerView.setAdapter(exerciseGroupListRecyclerAdapter);
    }

    public void getExerciseGroupList()
    {
        idListFromDB.removeAll(idListFromDB);
        nameListFromDB.removeAll(nameListFromDB);
        photoListFromDB.removeAll(photoListFromDB);

        String url = "http://batuhanbatu.net/gymkitwebservice/get_exercise_groups.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("group");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        idListFromDB.add(String.valueOf(row.getInt("id")));
                        nameListFromDB.add(row.getString("name"));
                        photoListFromDB.add(row.getString("photo"));

                    }

                    exerciseGroupListRecyclerAdapter.notifyDataSetChanged();

                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                    Toast.makeText(ExerciseGroupListActivity.this, "Error!", Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ExerciseGroupListActivity.this, "Error!", Toast.LENGTH_LONG).show();
            }
        });

        Volley.newRequestQueue(this).add(request);
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(ExerciseGroupListActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(ExerciseGroupListActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(ExerciseGroupListActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(ExerciseGroupListActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(ExerciseGroupListActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}