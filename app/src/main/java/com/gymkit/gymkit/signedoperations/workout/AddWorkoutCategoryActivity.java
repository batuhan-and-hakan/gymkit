package com.gymkit.gymkit.signedoperations.workout;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;

import java.util.HashMap;
import java.util.Map;

public class AddWorkoutCategoryActivity extends AppCompatActivity {

    SharedPreferences session;
    String workoutid = "";
    String creatorid = "";
    EditText categoryName;
    Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_workout_category);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Add Workout Category");

        Intent getintent = getIntent();
        workoutid = getintent.getStringExtra("workoutid");
        creatorid = getintent.getStringExtra("creatorid");

        categoryName = findViewById(R.id.category_name);
        saveButton = findViewById(R.id.saveButton);
    }

    public void save(View view)
    {
        if (categoryName.getText().toString().equals(""))
            Toast.makeText(this, "Category Name Must Not Be Empty!", Toast.LENGTH_SHORT).show();

        else
        {
            saveButton.setEnabled(false);

            String url = "http://batuhanbatu.net/gymkitwebservice/insert_workout_category.php";

            StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    if (response.contains("Successfully added"))
                    {

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AddWorkoutCategoryActivity.this);
                        alertDialogBuilder.setTitle("Added Successfully");
                        alertDialogBuilder.setCancelable(false);
                        alertDialogBuilder.setMessage("The category was added successfully!");

                        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent intent = new Intent(AddWorkoutCategoryActivity.this, WorkoutDetailActivity.class);
                                intent.putExtra("workoutid", workoutid);
                                intent.putExtra("creatorid", creatorid);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    }

                    else
                    {
                        Toast.makeText(AddWorkoutCategoryActivity.this, "Error!", Toast.LENGTH_LONG).show();
                        saveButton.setEnabled(true);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    saveButton.setEnabled(true);
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();

                    params.put("workoutid", String.valueOf(workoutid));
                    params.put("name", categoryName.getText().toString().replace("'", "~"));

                    return params;
                }
            };

            Volley.newRequestQueue(this).add(request);
        }


    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(AddWorkoutCategoryActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(AddWorkoutCategoryActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(AddWorkoutCategoryActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(AddWorkoutCategoryActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(AddWorkoutCategoryActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}