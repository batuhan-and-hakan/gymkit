package com.gymkit.gymkit.signedoperations.bodysize;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionDetailActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class BodySizeDetailActivity extends AppCompatActivity {

    SharedPreferences session;
    String id = "";
    String userid = "";
    TextView name, weight, fluid, oil, muscle;
    ImageView weightIcon, fluidIcon, oilIcon, muscleIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_body_size_detail);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Body Size Detail");

        Intent getintent = getIntent();
        id = getintent.getStringExtra("id");
        userid = getintent.getStringExtra("userid");

        name = findViewById(R.id.name);
        weight = findViewById(R.id.weight);
        fluid = findViewById(R.id.fluid);
        oil = findViewById(R.id.oil);
        muscle = findViewById(R.id.muscle);
        weightIcon = findViewById(R.id.weightIcon);
        fluidIcon = findViewById(R.id.fluidIcon);
        oilIcon = findViewById(R.id.oilIcon);
        muscleIcon = findViewById(R.id.muscleIcon);

        fillThePage();
    }

    public void fillThePage()
    {
        String url = "http://batuhanbatu.net/gymkitwebservice/get_body_size_analysis.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("body_size");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        name.setText(row.getString("name"));
                        weight.setText(row.getString("weight") + " kg");
                        fluid.setText(row.getString("fluid") + " %");
                        oil.setText(row.getString("oil") + " %");
                        muscle.setText(row.getString("muscle") + " %");

                        switch (row.getString("weightt"))
                        {
                            case "-1":
                                weightIcon.setImageResource(R.drawable.ic_arrow_down);
                                break;
                            case "0":
                                weightIcon.setImageResource(R.drawable.ic_check);
                                break;
                            case "1":
                                weightIcon.setImageResource(R.drawable.ic_arrow_up);
                                break;
                        }

                        switch (row.getString("fluidd"))
                        {
                            case "-1":
                                fluidIcon.setImageResource(R.drawable.ic_arrow_down);
                                break;
                            case "0":
                                fluidIcon.setImageResource(R.drawable.ic_check);
                                break;
                            case "1":
                                fluidIcon.setImageResource(R.drawable.ic_arrow_up);
                                break;
                        }

                        switch (row.getString("oill"))
                        {
                            case "-1":
                                oilIcon.setImageResource(R.drawable.ic_arrow_down);
                                break;
                            case "0":
                                oilIcon.setImageResource(R.drawable.ic_check);
                                break;
                            case "1":
                                oilIcon.setImageResource(R.drawable.ic_arrow_up);
                                break;
                        }

                        switch (row.getString("musclee"))
                        {
                            case "-1":
                                muscleIcon.setImageResource(R.drawable.ic_arrow_down);
                                break;
                            case "0":
                                muscleIcon.setImageResource(R.drawable.ic_check);
                                break;
                            case "1":
                                muscleIcon.setImageResource(R.drawable.ic_arrow_up);
                                break;
                        }
                    }

                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("id", id);

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.bodysizedetail_options_menu, menu);

        if (session.getString("usertype", "0").equals("0"))
        {
            menu.removeItem(R.id.edit);
            menu.removeItem(R.id.delete);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.edit) {

            Intent intent = new Intent(this, UpdateBodySizeActivity.class);
            intent.putExtra("id", id);
            intent.putExtra("userid", userid);
            startActivity(intent);
            finish();

        }

        else if (item.getItemId() == R.id.delete)
        {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BodySizeDetailActivity.this);
            alertDialogBuilder.setTitle("Are You Sure?");
            alertDialogBuilder.setMessage("Do you really want to delete this body size?");

            alertDialogBuilder.setCancelable(true);

            alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();

                    String url = "http://batuhanbatu.net/gymkitwebservice/delete_body_size.php";

                    StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            if (response.contains("Successfully deleted"))
                            {

                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BodySizeDetailActivity.this);
                                alertDialogBuilder.setTitle("Successfull!");
                                alertDialogBuilder.setCancelable(false);
                                alertDialogBuilder.setMessage("The body size was successfully deleted!");

                                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        Intent intent = new Intent(BodySizeDetailActivity.this, UsersBodySizeListActivity.class);
                                        intent.putExtra("userid", userid);
                                        startActivity(intent);
                                        finish();
                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            }

                            else
                                Toast.makeText(BodySizeDetailActivity.this, "Error!", Toast.LENGTH_LONG).show();


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }){
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<>();

                            params.put("id", id);

                            return params;
                        }
                    };

                    Volley.newRequestQueue(BodySizeDetailActivity.this).add(request);

                }
            });

            alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();

                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();

            alertDialog.show();

        }

        return super.onOptionsItemSelected(item);
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }

}