package com.gymkit.gymkit.signedoperations.bodysize;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListRecyclerAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BodySizeListActivity extends AppCompatActivity {

    SharedPreferences session;
    BodySizeListRecyclerAdapter bodySizeListRecyclerAdapter;
    ArrayList<String> idListFromDB;
    ArrayList<String> nameListFromDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_body_size_list);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);

        if (session.getString("usertype", "0").equals("1"))
        {
            Intent intent = new Intent(this, UserListToBodySizeActivity.class);
            startActivity(intent);
            finish();
        }

        getSupportActionBar().setTitle("Body Size List");

        idListFromDB = new ArrayList<>();
        nameListFromDB = new ArrayList<>();

        getBodySize();

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        bodySizeListRecyclerAdapter = new BodySizeListRecyclerAdapter(BodySizeListActivity.this, String.valueOf(session.getInt("id", 0)), idListFromDB, nameListFromDB);
        recyclerView.setAdapter(bodySizeListRecyclerAdapter);
    }

    public void getBodySize()
    {
        idListFromDB.removeAll(idListFromDB);
        nameListFromDB.removeAll(nameListFromDB);

        String url = "http://batuhanbatu.net/gymkitwebservice/get_body_size_by_userid.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("body_size");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        idListFromDB.add(String.valueOf(row.getInt("id")));
                        nameListFromDB.add(row.getString("name").replace("~", "'"));
                    }

                    bodySizeListRecyclerAdapter.notifyDataSetChanged();
                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("userid", String.valueOf(session.getInt("id", 0)));

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.bodysizelist_options_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.compare)
        {
            Intent intent = new Intent(this, FirstBodySizeToCompareActivity.class);
            intent.putExtra("userid", String.valueOf(session.getInt("id", 0)));
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(this, BodySizeListActivity.class);
        startActivity(intent);
        finish();
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }
}