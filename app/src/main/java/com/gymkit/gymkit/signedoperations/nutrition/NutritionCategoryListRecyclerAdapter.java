package com.gymkit.gymkit.signedoperations.nutrition;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.workout.UsersWorkoutListActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutDetailActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NutritionCategoryListRecyclerAdapter extends RecyclerView.Adapter<com.gymkit.gymkit.signedoperations.nutrition.NutritionCategoryListRecyclerAdapter.PostHolder> {

    private Context context;
    private String nutritionid;
    private String creatorid;
    private ArrayList<String> idList;
    private ArrayList<String> nameList;


    public NutritionCategoryListRecyclerAdapter(Context context, String nutritionid, String creatorid, ArrayList<String> idList, ArrayList<String> nameList) {
        this.context = context;
        this.nutritionid = nutritionid;
        this.creatorid = creatorid;
        this.idList = idList;
        this.nameList = nameList;
    }

    @NonNull
    @Override
    public com.gymkit.gymkit.signedoperations.nutrition.NutritionCategoryListRecyclerAdapter.PostHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.workout_category_list_recycler_row,parent,false);
        return new com.gymkit.gymkit.signedoperations.nutrition.NutritionCategoryListRecyclerAdapter.PostHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final com.gymkit.gymkit.signedoperations.nutrition.NutritionCategoryListRecyclerAdapter.PostHolder holder, final int position) {

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, AddMealActivity.class);
                intent.putExtra("categoryid", idList.get(position));
                intent.putExtra("nutritionid", nutritionid);
                intent.putExtra("creatorid", creatorid);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);

            }
        });

        holder.nameTextView.setText(nameList.get(position));

    }

    @Override
    public int getItemCount() {
        return idList.size();
    }

    class PostHolder extends RecyclerView.ViewHolder {

        ConstraintLayout container;
        TextView nameTextView;

        public PostHolder(@NonNull final View itemView) {
            super(itemView);

            container = itemView.findViewById(R.id.container);
            nameTextView = itemView.findViewById(R.id.name);

        }
    }
}