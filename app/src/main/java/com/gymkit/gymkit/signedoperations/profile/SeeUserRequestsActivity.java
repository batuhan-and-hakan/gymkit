package com.gymkit.gymkit.signedoperations.profile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SeeUserRequestsActivity extends AppCompatActivity {

    SharedPreferences session;
    UserRequestRecyclerAdapter userRequestRecyclerAdapter;
    ArrayList<String> idListFromDB;
    ArrayList<String> fnameListFromDB;
    ArrayList<String> lnameListFromDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_user_requests);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("User Requests");

        idListFromDB = new ArrayList<>();
        fnameListFromDB = new ArrayList<>();
        lnameListFromDB = new ArrayList<>();

        getRequests();

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        userRequestRecyclerAdapter = new UserRequestRecyclerAdapter(this, idListFromDB, fnameListFromDB, lnameListFromDB);
        recyclerView.setAdapter(userRequestRecyclerAdapter);

    }

    public void getRequests()
    {
        idListFromDB.removeAll(idListFromDB);
        fnameListFromDB.removeAll(fnameListFromDB);
        lnameListFromDB.removeAll(lnameListFromDB);

        String url = "http://batuhanbatu.net/gymkitwebservice/get_user_requests.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("user");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        idListFromDB.add(String.valueOf(row.getInt("id")));
                        fnameListFromDB.add(row.getString("fname"));
                        lnameListFromDB.add(row.getString("lname"));

                    }

                    userRequestRecyclerAdapter.notifyDataSetChanged();
                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("gymid", String.valueOf(session.getInt("gymid", 0)));

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(SeeUserRequestsActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(SeeUserRequestsActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(SeeUserRequestsActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(SeeUserRequestsActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(SeeUserRequestsActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}
