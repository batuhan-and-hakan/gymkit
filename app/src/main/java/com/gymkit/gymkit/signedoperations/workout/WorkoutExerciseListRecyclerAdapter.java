package com.gymkit.gymkit.signedoperations.workout;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.workout.exercisedetail.ExerciseDetailActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WorkoutExerciseListRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public boolean editMode = false;

    private Context context;
    public boolean usertype = false;
    private String userid;
    private String creatorid;
    private String workoutid;
    private ArrayList<String> dataTypeList;
    private ArrayList<String> idList;
    private ArrayList<String> weidList;
    private ArrayList<String> nameList;
    private ArrayList<String> gifList;
    private ArrayList<String> setList;
    private ArrayList<String> repList;


    public WorkoutExerciseListRecyclerAdapter(Context context, boolean usertype, String userid, String creatorid, String workoutid, ArrayList<String> dataTypeList, ArrayList<String> idList, ArrayList<String> weidList, ArrayList<String> nameList, ArrayList<String> gifList, ArrayList<String> setList, ArrayList<String> repList) {
        this.context = context;
        this.usertype = usertype;
        this.userid = userid;
        this.creatorid = creatorid;
        this.workoutid = workoutid;
        this.dataTypeList = dataTypeList;
        this.idList = idList;
        this.weidList = weidList;
        this.nameList = nameList;
        this.gifList = gifList;
        this.setList = setList;
        this.repList = repList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        switch(viewType){
            case 0: //category
            {
                View listItem = layoutInflater.inflate(R.layout.workout_category_recycler_row, parent, false);
                RecyclerView.ViewHolder viewHolder = new CategoryViewHolder(listItem);
                return viewHolder;
            }
            case 1: //exercise
            {
                View listItem = layoutInflater.inflate(R.layout.workout_exercise_recycler_row, parent, false);
                RecyclerView.ViewHolder viewHolder = new ExerciseViewHolder(listItem);
                return viewHolder;
            }
        }

        return null;

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        switch (holder.getItemViewType()) {

            case 0: //category
                CategoryViewHolder category = (CategoryViewHolder) holder;

                category.textView.setText(nameList.get(position));


                if (editMode && (usertype || userid.equals(creatorid)))
                {
                    category.editButton.setVisibility(View.VISIBLE);
                    category.deleteButton.setVisibility(View.VISIBLE);
                }

                else
                {
                    category.editButton.setVisibility(View.GONE);
                    category.deleteButton.setVisibility(View.GONE);
                }

                category.editButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, UpdateWorkoutCategoryActivity.class);
                        intent.putExtra("id", idList.get(position));
                        intent.putExtra("workoutid", workoutid);
                        intent.putExtra("creatorid", creatorid);
                        context.startActivity(intent);

                    }
                });

                category.deleteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                        alertDialogBuilder.setTitle("Are You Sure?");
                        alertDialogBuilder.setMessage("Do you really want to delete this category?");

                        alertDialogBuilder.setCancelable(true);

                        alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();

                                String url = "http://batuhanbatu.net/gymkitwebservice/delete_category.php";

                                StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {

                                        if (response.contains("Successfully deleted"))
                                        {

                                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                            alertDialogBuilder.setTitle("Successfull!");
                                            alertDialogBuilder.setCancelable(true);
                                            alertDialogBuilder.setMessage("The category was successfully deleted!");

                                            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                    Intent intent = new Intent(context, WorkoutDetailActivity.class);
                                                    intent.putExtra("workoutid", workoutid);
                                                    intent.putExtra("creatorid", creatorid);
                                                    context.startActivity(intent);

                                                }
                                            });

                                            AlertDialog alertDialog = alertDialogBuilder.create();
                                            alertDialog.show();

                                        }

                                        else
                                            Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();


                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {

                                    }
                                }){
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {

                                        Map<String, String> params = new HashMap<>();

                                        params.put("id", String.valueOf(idList.get(position)));

                                        return params;
                                    }
                                };

                                Volley.newRequestQueue(context).add(request);

                            }
                        });

                        alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();

                        alertDialog.show();

                    }
                });

                break;

            case 1: //exercise
                ExerciseViewHolder exercise = (ExerciseViewHolder) holder;

                exercise.exercise_name.setText(nameList.get(position));
                exercise.set_rep_num.setText(setList.get(position) + " x " + repList.get(position) + " Rpt.");
                exercise.exercise_gif.loadUrl("http://batuhanbatu.net/gymkitwebservice/gif_maker.php?url=" + gifList.get(position));
                exercise.exercise_gif.setBackgroundColor(Color.TRANSPARENT);


                if (editMode && (usertype || userid.equals(creatorid)))
                {
                    exercise.editButton.setVisibility(View.VISIBLE);
                    exercise.deleteButton.setVisibility(View.VISIBLE);
                }

                else
                {
                    exercise.editButton.setVisibility(View.GONE);
                    exercise.deleteButton.setVisibility(View.GONE);
                }

                exercise.editButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, UpdateExerciseActivity.class);
                        intent.putExtra("id", weidList.get(position));
                        intent.putExtra("workoutid", workoutid);
                        intent.putExtra("creatorid", creatorid);
                        context.startActivity(intent);

                    }
                });

                exercise.deleteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                        alertDialogBuilder.setTitle("Are You Sure?");
                        alertDialogBuilder.setMessage("Do you really want to delete this exercise?");

                        alertDialogBuilder.setCancelable(true);

                        alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();

                                String url = "http://batuhanbatu.net/gymkitwebservice/delete_exercise.php";

                                StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {

                                        if (response.contains("Successfully deleted"))
                                        {
                                            dataTypeList.remove(position);
                                            idList.remove(position);
                                            weidList.remove(position);
                                            nameList.remove(position);
                                            gifList.remove(position);
                                            setList.remove(position);
                                            repList.remove(position);
                                            notifyDataSetChanged();

                                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                            alertDialogBuilder.setTitle("Successfull!");
                                            alertDialogBuilder.setCancelable(true);
                                            alertDialogBuilder.setMessage("The exercise was successfully deleted!");

                                            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                    dialog.dismiss();

                                                }
                                            });

                                            AlertDialog alertDialog = alertDialogBuilder.create();
                                            alertDialog.show();

                                        }

                                        else
                                            Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();


                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {

                                    }
                                }){
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {

                                        Map<String, String> params = new HashMap<>();

                                        params.put("id", String.valueOf(weidList.get(position)));

                                        return params;
                                    }
                                };

                                Volley.newRequestQueue(context).add(request);

                            }
                        });

                        alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();

                        alertDialog.show();

                    }
                });

                exercise.exercise_container.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, ExerciseDetailActivity.class);
                        intent.putExtra("id", idList.get(position));
                        context.startActivity(intent);

                    }
                });

                break;
        }

    }

    @Override
    public int getItemViewType(int position) {
        return Integer.parseInt(dataTypeList.get(position));
    }

    @Override
    public int getItemCount() {
        return idList.size();
    }

    public static class CategoryViewHolder extends RecyclerView.ViewHolder {

        public TextView textView;
        public ImageView editButton, deleteButton;

        public CategoryViewHolder(@NonNull View itemView) {

            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.textView);
            editButton = (ImageView) itemView.findViewById(R.id.editButton);
            deleteButton = (ImageView) itemView.findViewById(R.id.deleteButton);

        }
    }
    public static class ExerciseViewHolder extends RecyclerView.ViewHolder {

        public ConstraintLayout exercise_container;
        public WebView exercise_gif;
        public TextView exercise_name, set_rep_num;
        public ImageView editButton, deleteButton;


        public ExerciseViewHolder(@NonNull View itemView) {

            super(itemView);
            exercise_container = (ConstraintLayout) itemView.findViewById(R.id.exercise_container);
            exercise_gif = (WebView) itemView.findViewById(R.id.exercise_gif);
            exercise_name = (TextView) itemView.findViewById(R.id.exercise_name);
            set_rep_num = (TextView) itemView.findViewById(R.id.set_rep_num);
            editButton = (ImageView) itemView.findViewById(R.id.editButton);
            deleteButton = (ImageView) itemView.findViewById(R.id.deleteButton);

        }

    }

}