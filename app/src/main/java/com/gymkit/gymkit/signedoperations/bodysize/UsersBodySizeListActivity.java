package com.gymkit.gymkit.signedoperations.bodysize;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.AddWorkoutActivity;
import com.gymkit.gymkit.signedoperations.workout.UserListToWorkoutActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UsersBodySizeListActivity extends AppCompatActivity {

    SharedPreferences session;
    BodySizeListRecyclerAdapter bodySizeListRecyclerAdapter;
    ArrayList<String> idListFromDB;
    ArrayList<String> nameListFromDB;
    String userid = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_body_size_list);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);

        getSupportActionBar().setTitle("User's Body Size List");

        idListFromDB = new ArrayList<>();
        nameListFromDB = new ArrayList<>();

        Intent getintent = getIntent();
        userid = getintent.getStringExtra("userid");

        getBodySize();

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        bodySizeListRecyclerAdapter = new BodySizeListRecyclerAdapter(UsersBodySizeListActivity.this, userid, idListFromDB, nameListFromDB);
        recyclerView.setAdapter(bodySizeListRecyclerAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.usersbodysizelist_options_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.add_body_size) {

            Intent intent = new Intent(this, AddBodySizeActivity.class);
            intent.putExtra("userid", userid);
            startActivity(intent);

        }

        else if (item.getItemId() == R.id.change_height)
        {
            Intent intent = new Intent(this, ChangeHeightActivity.class);
            intent.putExtra("userid", userid);
            startActivity(intent);
        }

        else if (item.getItemId() == R.id.compare)
        {
            Intent intent = new Intent(this, FirstBodySizeToCompareActivity.class);
            intent.putExtra("userid", userid);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    public void getBodySize()
    {
        idListFromDB.removeAll(idListFromDB);
        nameListFromDB.removeAll(nameListFromDB);

        final String url = "http://batuhanbatu.net/gymkitwebservice/get_body_size_by_userid.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("body_size");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        idListFromDB.add(String.valueOf(row.getInt("id")));
                        nameListFromDB.add(row.getString("name").replace("~", "'"));
                    }

                    bodySizeListRecyclerAdapter.notifyDataSetChanged();
                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("userid", userid);

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }
}