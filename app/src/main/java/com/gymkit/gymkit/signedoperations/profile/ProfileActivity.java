package com.gymkit.gymkit.signedoperations.profile;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.article.WriteArticleActivity;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;
import com.gymkit.gymkit.unsignedoperations.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity {

    SharedPreferences session;

    LinearLayout gymCodeLayout;
    TextView fnameTextView, emailTextView, genderTextView, phoneTextView, bdateTextView;
    EditText gymCodeEditText;
    Button enterTheGymButton, manageGymUsersButton, seeUserRequestsButton, leaveGymButton, generateCodeButton;

    int gymid = 0;
    String gymcode = "";
    boolean usertype = false;
    boolean activation = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Profile");

        fnameTextView = findViewById(R.id.fname);
        emailTextView = findViewById(R.id.email);
        genderTextView = findViewById(R.id.gender);
        phoneTextView = findViewById(R.id.phone);
        bdateTextView = findViewById(R.id.bdate);
        gymCodeEditText = findViewById(R.id.gymCodeEditText);
        enterTheGymButton = findViewById(R.id.enterTheGymButton);
        gymCodeLayout = findViewById(R.id.gymCodeLayout);
        manageGymUsersButton = findViewById(R.id.manageGymUsers);
        seeUserRequestsButton = findViewById(R.id.seeUserRequests);
        leaveGymButton = findViewById(R.id.leaveGym);
        generateCodeButton = findViewById(R.id.generateCode);

        getMyInfos();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.profile_options_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.edit)
        {
            Intent intent = new Intent(ProfileActivity.this, EditProfileActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    public void getMyInfos()
    {
        String url = "http://batuhanbatu.net/gymkitwebservice/get_user_by_id.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("user");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        fnameTextView.setText(row.getString("fname") + " " + row.getString("lname"));
                        emailTextView.setText(row.getString("email"));
                        phoneTextView.setText(row.getString("phone"));

                        if (row.getString("activation").equals("1"))
                            activation = true;

                        if (row.getString("gender").equals("1"))
                            genderTextView.setText("Male");

                        else
                            genderTextView.setText("Female");

                        bdateTextView.setText(row.getString("bday"));
                        gymid = row.getInt("gymid");

                        if (gymid == 0)
                        {
                            gymCodeLayout.setVisibility(View.VISIBLE);
                        }

                        else if (activation)
                        {
                            leaveGymButton.setVisibility(View.VISIBLE);
                        }

                        if (row.getString("usertype").equals("1"))
                            usertype = true;

                        if (usertype && activation)
                        {
                            manageGymUsersButton.setVisibility(View.VISIBLE);
                            seeUserRequestsButton.setVisibility(View.VISIBLE);
                            generateCodeButton.setVisibility(View.VISIBLE);
                        }

                    }

                }

                catch (JSONException e)
                {
                    e.printStackTrace();

                    Toast.makeText(ProfileActivity.this, "Your login information(s) is wrong!", Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("id", String.valueOf(session.getInt("id", 0)));

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }

    public void seeUserRequests(View view)
    {
        Intent intent = new Intent(ProfileActivity.this, SeeUserRequestsActivity.class);
        startActivity(intent);
    }

    public void manageGymUsers(View view)
    {
        Intent intent = new Intent(ProfileActivity.this, ManageGymUsersActivity.class);
        startActivity(intent);
    }

    public void enterTheGym(View view)
    {

        enterTheGymButton.setEnabled(false);

        if (gymCodeEditText.getText().toString().equals(""))
        {
            Toast.makeText(this, "Gym Code can not be empty!", Toast.LENGTH_SHORT).show();
            enterTheGymButton.setEnabled(true);
        }

        else
        {
            String url = "http://batuhanbatu.net/gymkitwebservice/enter_gym.php";

            StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    if (response.contains("Successfully added"))
                        Toast.makeText(ProfileActivity.this, "Request was successfully gone to the gym! Confirmation will be from your gym trainers..", Toast.LENGTH_LONG).show();

                    else
                        Toast.makeText(ProfileActivity.this, "This code is not valid!", Toast.LENGTH_LONG).show();

                    enterTheGymButton.setEnabled(true);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(ProfileActivity.this, "Error!", Toast.LENGTH_LONG).show();
                    enterTheGymButton.setEnabled(true);
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();

                    params.put("userid", String.valueOf(session.getInt("id", 0)));
                    params.put("code", gymCodeEditText.getText().toString());

                    return params;
                }
            };

            Volley.newRequestQueue(this).add(request);

        }

    }

    public void generateCode(View view)
    {

        if (gymid == 0 || !usertype)
            Toast.makeText(this, "This operation is not valid for you", Toast.LENGTH_SHORT).show();

        else
        {
            String url = "http://batuhanbatu.net/gymkitwebservice/generate_code.php";

            StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try
                    {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray result = jsonObject.getJSONArray("code");

                        for (int i = 0; i < result.length(); i++)
                        {
                            JSONObject row = result.getJSONObject(i);

                            gymcode = row.getString("code");

                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProfileActivity.this);
                            alertDialogBuilder.setTitle("Code is generated successfully!");
                            alertDialogBuilder.setMessage("Generated code to enter your gym is: " + row.getString("code"));

                            alertDialogBuilder.setCancelable(true);

                            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();

                                }
                            });

                            alertDialogBuilder.setNeutralButton("COPY THE CODE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();

                                    ClipboardManager clipboard = (ClipboardManager) getSystemService(ProfileActivity.CLIPBOARD_SERVICE);
                                    ClipData clip = ClipData.newPlainText("Gym Code", "Gym Code: " + gymcode);
                                    clipboard.setPrimaryClip(clip);
                                    Toast.makeText(ProfileActivity.this, "The Code has been copied", Toast.LENGTH_LONG).show();

                                }
                            });

                            alertDialogBuilder.setNegativeButton("SHARE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    Intent intent = new Intent(android.content.Intent.ACTION_SEND);

                                    String shareBody = "Gym Code: " + gymcode;
                                    String shareSubject = "Gym Code";

                                    intent.setType("text/plain");

                                    intent.putExtra(android.content.Intent.EXTRA_SUBJECT, shareSubject);
                                    intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);

                                    startActivity(Intent.createChooser(intent, "Share the code to:"));

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();

                            alertDialog.show();

                        }

                    }

                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(ProfileActivity.this, "Error!", Toast.LENGTH_LONG).show();
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();

                    params.put("gymid", String.valueOf(gymid));

                    return params;
                }
            };

            Volley.newRequestQueue(this).add(request);
        }


    }

    public void leaveGym(View view)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProfileActivity.this);
        alertDialogBuilder.setTitle("Are You Sure?");
        alertDialogBuilder.setMessage("Do you really want to leave your gym?");

        alertDialogBuilder.setCancelable(true);

        alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                String url = "http://batuhanbatu.net/gymkitwebservice/leave_gym.php";

                StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response.contains("Successfully deleted"))
                        {
                            session.edit().clear().apply();

                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProfileActivity.this);
                            alertDialogBuilder.setTitle("You were successfully removed from the gym!");
                            alertDialogBuilder.setCancelable(false);
                            alertDialogBuilder.setMessage("Now, you must login again..");

                            alertDialogBuilder.setPositiveButton("Go To The Login Page", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    Intent intentToSignOut = new Intent(ProfileActivity.this, MainActivity.class);
                                    intentToSignOut.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intentToSignOut);
                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        }

                        else
                            Toast.makeText(ProfileActivity.this, "Error!", Toast.LENGTH_LONG).show();

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ProfileActivity.this, "Error!", Toast.LENGTH_LONG).show();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<>();

                        params.put("userid", String.valueOf(session.getInt("id", 0)));

                        return params;
                    }
                };

                Volley.newRequestQueue(ProfileActivity.this).add(request);

            }
        });

        alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }

    public void signOut(View view)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProfileActivity.this);
        alertDialogBuilder.setTitle("Are You Sure?");
        alertDialogBuilder.setMessage("Do you really want to sign out?");

        alertDialogBuilder.setCancelable(true);

        alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                session.edit().clear().apply();

                Intent intentToSignOut = new Intent(ProfileActivity.this, MainActivity.class);
                intentToSignOut.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentToSignOut);
                finish();

            }
        });

        alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }

    public void changePassword(View view)
    {
        Intent intent = new Intent(ProfileActivity.this, ChangePasswordActivity.class);
        startActivity(intent);
    }

    public void savedArticles(View view)
    {
        Intent intent = new Intent(ProfileActivity.this, SavedArticleActivity.class);
        startActivity(intent);
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(ProfileActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(ProfileActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(ProfileActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(ProfileActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(ProfileActivity.this, ProfileActivity.class);
        startActivity(intent);
        finish();
    }
}
