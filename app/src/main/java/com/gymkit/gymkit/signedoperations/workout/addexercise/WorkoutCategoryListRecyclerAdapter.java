package com.gymkit.gymkit.signedoperations.workout.addexercise;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.workout.UsersWorkoutListActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutDetailActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WorkoutCategoryListRecyclerAdapter extends RecyclerView.Adapter<com.gymkit.gymkit.signedoperations.workout.addexercise.WorkoutCategoryListRecyclerAdapter.PostHolder> {

    private Context context;
    private String exerciseid;
    private String workoutid;
    private String creatorid;
    private ArrayList<String> idList;
    private ArrayList<String> nameList;


    public WorkoutCategoryListRecyclerAdapter(Context context, String exerciseid, String workoutid, String creatorid, ArrayList<String> idList, ArrayList<String> nameList) {
        this.context = context;
        this.exerciseid = exerciseid;
        this.workoutid = workoutid;
        this.creatorid = creatorid;
        this.idList = idList;
        this.nameList = nameList;
    }

    @NonNull
    @Override
    public com.gymkit.gymkit.signedoperations.workout.addexercise.WorkoutCategoryListRecyclerAdapter.PostHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.workout_category_list_recycler_row,parent,false);
        return new com.gymkit.gymkit.signedoperations.workout.addexercise.WorkoutCategoryListRecyclerAdapter.PostHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final com.gymkit.gymkit.signedoperations.workout.addexercise.WorkoutCategoryListRecyclerAdapter.PostHolder holder, final int position) {

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle("Are You Sure?");
                alertDialogBuilder.setMessage("Do you really want to add this exercise to this workout?\n\n\nNumber of Set and Repetition will automatically be 0. You can change it after confirmation from 'Edit' menu!");

                alertDialogBuilder.setCancelable(true);

                alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                        String url = "http://batuhanbatu.net/gymkitwebservice/insert_workout_exercise.php";

                        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                if (response.contains("Successfully added"))
                                {
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                    alertDialogBuilder.setTitle("Successfull!");
                                    alertDialogBuilder.setCancelable(false);
                                    alertDialogBuilder.setMessage("The exercise was successfully added to the workout!");

                                    alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            Intent intent = new Intent(context, WorkoutDetailActivity.class);
                                            intent.putExtra("workoutid", workoutid);
                                            intent.putExtra("creatorid", creatorid);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            context.startActivity(intent);

                                        }
                                    });

                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();

                                }

                                else
                                    Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();


                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {

                                Map<String, String> params = new HashMap<>();

                                params.put("workoutid", workoutid);
                                params.put("exerciseid", exerciseid);
                                params.put("categoryid", idList.get(position));

                                return params;
                            }
                        };

                        Volley.newRequestQueue(context).add(request);

                    }
                });

                alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();

                alertDialog.show();

            }
        });

        holder.nameTextView.setText(nameList.get(position));

    }

    @Override
    public int getItemCount() {
        return idList.size();
    }

    class PostHolder extends RecyclerView.ViewHolder {

        ConstraintLayout container;
        TextView nameTextView;

        public PostHolder(@NonNull final View itemView) {
            super(itemView);

            container = itemView.findViewById(R.id.container);
            nameTextView = itemView.findViewById(R.id.name);

        }
    }
}