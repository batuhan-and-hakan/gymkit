package com.gymkit.gymkit.signedoperations.nutrition;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NutritionDetailActivity extends AppCompatActivity {

    SharedPreferences session;
    String nutritionid = "";
    String creatorid = "";
    boolean usertype = false;
    NutritionDetailListRecyclerAdapter nutritionDetailListRecyclerAdapter;
    ArrayList<String> dataTypeListFromDB;
    ArrayList<String> idListFromDB;
    ArrayList<String> nameListFromDB;
    ArrayList<String> contentListFromDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nutrition_detail);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Nutrition Detail");

        dataTypeListFromDB = new ArrayList<>();
        idListFromDB = new ArrayList<>();
        nameListFromDB = new ArrayList<>();
        contentListFromDB = new ArrayList<>();

        if (session.getString("usertype", "0").equals("1"))
            usertype = true;
        else
            usertype = false;

        Intent getintent = getIntent();
        nutritionid = getintent.getStringExtra("nutritionid");
        creatorid = getintent.getStringExtra("creatorid");

        getNutritionDetails();

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        nutritionDetailListRecyclerAdapter = new NutritionDetailListRecyclerAdapter(this, usertype, String.valueOf(session.getInt("id", 0)), creatorid, nutritionid, dataTypeListFromDB, idListFromDB, nameListFromDB, contentListFromDB);
        recyclerView.setAdapter(nutritionDetailListRecyclerAdapter);
    }

    public void getNutritionDetails()
    {
        dataTypeListFromDB.removeAll(dataTypeListFromDB);
        idListFromDB.removeAll(idListFromDB);
        nameListFromDB.removeAll(nameListFromDB);
        contentListFromDB.removeAll(contentListFromDB);

        String url = "http://batuhanbatu.net/gymkitwebservice/get_nutrition_details.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("detail");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        if (String.valueOf(row.getInt("type")).equals("0"))
                        {
                            dataTypeListFromDB.add("0");
                            idListFromDB.add(String.valueOf(row.getInt("id")));
                            nameListFromDB.add(row.getString("name").replace("~", "'"));
                            contentListFromDB.add("");
                        }
                        else
                        {
                            dataTypeListFromDB.add("1");
                            idListFromDB.add(String.valueOf(row.getInt("id")));
                            nameListFromDB.add(row.getString("name"));
                            contentListFromDB.add(row.getString("content").replace("~", "'"));
                        }

                    }

                    nutritionDetailListRecyclerAdapter.notifyDataSetChanged();
                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("nutritionid", nutritionid);

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.nutritiondetail_options_menu, menu);

        if (!usertype && !String.valueOf(session.getInt("id", 0)).equals(creatorid))
        {
            menu.removeItem(R.id.add_meal);
            menu.removeItem(R.id.add_category);
            menu.removeItem(R.id.edit);
        }

        if (!usertype)
        {
            menu.removeItem(R.id.assign_to_user);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.assign_to_user) {

            Intent intent = new Intent(NutritionDetailActivity.this, AssignNutritionActivity.class);
            intent.putExtra("nutritionid", nutritionid);
            intent.putExtra("creatorid", creatorid);
            startActivity(intent);
        }

        else if (item.getItemId() == R.id.add_meal) {

            Intent intent = new Intent(NutritionDetailActivity.this, NutritionCategoryListActivity.class);
            intent.putExtra("nutritionid", nutritionid);
            intent.putExtra("creatorid", creatorid);
            startActivity(intent);
            finish();

        }

        else if (item.getItemId() == R.id.add_category)
        {
            Intent intent = new Intent(NutritionDetailActivity.this, AddNutritionCategoryActivity.class);
            intent.putExtra("nutritionid", nutritionid);
            intent.putExtra("creatorid", creatorid);
            startActivity(intent);
            finish();
        }

        else if (item.getItemId() == R.id.edit)
        {
            if (nutritionDetailListRecyclerAdapter.editMode == true)
                nutritionDetailListRecyclerAdapter.editMode = false;
            else
                nutritionDetailListRecyclerAdapter.editMode = true;

            nutritionDetailListRecyclerAdapter.notifyDataSetChanged();
        }

        return super.onOptionsItemSelected(item);
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(NutritionDetailActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(NutritionDetailActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(NutritionDetailActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(NutritionDetailActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(NutritionDetailActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}