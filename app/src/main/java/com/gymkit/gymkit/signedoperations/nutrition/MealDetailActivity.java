package com.gymkit.gymkit.signedoperations.nutrition;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;
import com.gymkit.gymkit.signedoperations.workout.exercisedetail.ExerciseDetailActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MealDetailActivity extends AppCompatActivity {

    SharedPreferences session;
    String id = "";
    String nutritionid = "";
    String creatorid = "";
    boolean usertype = false;

    TextView meal_name, meal_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meal_detail);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Meal Details");

        if (session.getString("usertype", "0").equals("1"))
            usertype = true;
        else
            usertype = false;

        Intent getintent = getIntent();
        id = getintent.getStringExtra("id");
        nutritionid = getintent.getStringExtra("nutritionid");
        creatorid = getintent.getStringExtra("creatorid");

        meal_name = findViewById(R.id.meal_name);
        meal_content = findViewById(R.id.meal_content);

        getMealDetails();
    }

    public void getMealDetails()
    {
        String url = "http://batuhanbatu.net/gymkitwebservice/get_meal_by_id.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("meal");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        meal_name.setText(row.getString("name").replace("~", "'"));
                        meal_content.setText(row.getString("content").replace("~", "'"));
                    }

                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("id", id);

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.mealdetail_options_menu, menu);

        if (!usertype && !String.valueOf(session.getInt("id", 0)).equals(creatorid))
        {
            menu.removeItem(R.id.edit);
            menu.removeItem(R.id.delete);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.edit) {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MealDetailActivity.this);
            alertDialogBuilder.setTitle("Which one?");
            alertDialogBuilder.setMessage("Do you want to change the category of the meal or do you want to edit the details of the meal?");

            alertDialogBuilder.setCancelable(true);

            alertDialogBuilder.setPositiveButton("EDIT DETAILS", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Intent intent = new Intent(MealDetailActivity.this, UpdateMealActivity.class);
                    intent.putExtra("id", id);
                    intent.putExtra("nutritionid", nutritionid);
                    intent.putExtra("creatorid", creatorid);
                    startActivity(intent);

                }
            });

            alertDialogBuilder.setNegativeButton("CHANGE CATEGORY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Intent intent = new Intent(MealDetailActivity.this, NutritionCategoryUpdateListActivity.class);
                    intent.putExtra("mealid", id);
                    intent.putExtra("nutritionid", nutritionid);
                    intent.putExtra("creatorid", creatorid);
                    startActivity(intent);

                }
            });

            alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();

                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();

            alertDialog.show();

        }

        else if (item.getItemId() == R.id.delete) {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MealDetailActivity.this);
            alertDialogBuilder.setTitle("Are You Sure?");
            alertDialogBuilder.setMessage("Do you really want to delete this meal?");

            alertDialogBuilder.setCancelable(true);

            alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();

                    String url = "http://batuhanbatu.net/gymkitwebservice/delete_meal.php";

                    StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            if (response.contains("Successfully deleted"))
                            {

                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MealDetailActivity.this);
                                alertDialogBuilder.setTitle("Successfull!");
                                alertDialogBuilder.setCancelable(false);
                                alertDialogBuilder.setMessage("The meal was successfully deleted!");

                                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        Intent intent = new Intent(MealDetailActivity.this, NutritionDetailActivity.class);
                                        intent.putExtra("nutritionid", nutritionid);
                                        intent.putExtra("creatorid", creatorid);
                                        startActivity(intent);

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            }

                            else
                                Toast.makeText(MealDetailActivity.this, "Error!", Toast.LENGTH_LONG).show();


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }){
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<>();

                            params.put("id", id);

                            return params;
                        }
                    };

                    Volley.newRequestQueue(MealDetailActivity.this).add(request);

                }
            });

            alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();

                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();

            alertDialog.show();

        }

        return super.onOptionsItemSelected(item);
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(MealDetailActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(MealDetailActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(MealDetailActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(MealDetailActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(MealDetailActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}