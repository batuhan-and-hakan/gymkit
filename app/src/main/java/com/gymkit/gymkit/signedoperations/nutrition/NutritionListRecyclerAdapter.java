package com.gymkit.gymkit.signedoperations.nutrition;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NutritionListRecyclerAdapter extends RecyclerView.Adapter<com.gymkit.gymkit.signedoperations.nutrition.NutritionListRecyclerAdapter.PostHolder> {

    private Context context;
    public boolean usertype = false;
    private String userid;
    public boolean editMode = false;
    private ArrayList<String> idList;
    private ArrayList<String> nameList;
    private ArrayList<String> creatorIdList;


    public NutritionListRecyclerAdapter(Context context, boolean usertype, String userid, ArrayList<String> idList, ArrayList<String> nameList, ArrayList<String> creatorIdList) {
        this.context = context;
        this.usertype = usertype;
        this.userid = userid;
        this.idList = idList;
        this.nameList = nameList;
        this.creatorIdList = creatorIdList;
    }

    @NonNull
    @Override
    public com.gymkit.gymkit.signedoperations.nutrition.NutritionListRecyclerAdapter.PostHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.nutrition_list_recycler_row,parent,false);
        return new com.gymkit.gymkit.signedoperations.nutrition.NutritionListRecyclerAdapter.PostHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final com.gymkit.gymkit.signedoperations.nutrition.NutritionListRecyclerAdapter.PostHolder holder, final int position) {

        if (!userid.equals(creatorIdList.get(position)))
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.container.setBackgroundColor(context.getColor(R.color.colorList));
            }
            else {
                holder.container.setBackgroundColor(context.getResources().getColor(R.color.colorList));
            }
        }

        else
            holder.container.setBackgroundColor(Color.WHITE);


        if (editMode && (usertype || userid.equals(creatorIdList.get(position))))
        {
            holder.edit.setVisibility(View.VISIBLE);
            holder.delete.setVisibility(View.VISIBLE);
        }

        else
        {
            holder.edit.setVisibility(View.GONE);
            holder.delete.setVisibility(View.GONE);
        }

        holder.nameTextView.setText(nameList.get(position));

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, NutritionDetailActivity.class);
                intent.putExtra("nutritionid", idList.get(position));
                intent.putExtra("creatorid", creatorIdList.get(position));
                context.startActivity(intent);

            }
        });

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, UpdateNutritionNameActivity.class);
                intent.putExtra("id", idList.get(position));
                context.startActivity(intent);

            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle("Are You Sure?");
                alertDialogBuilder.setMessage("Do you really want to delete this nutrition?");

                alertDialogBuilder.setCancelable(true);

                alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                        String url = "http://batuhanbatu.net/gymkitwebservice/delete_nutrition.php";

                        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                if (response.contains("Successfully deleted"))
                                {
                                    idList.remove(position);
                                    nameList.remove(position);
                                    creatorIdList.remove(position);
                                    notifyDataSetChanged();

                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                    alertDialogBuilder.setTitle("Successfull!");
                                    alertDialogBuilder.setCancelable(true);
                                    alertDialogBuilder.setMessage("The nutrition was successfully deleted!");

                                    alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {


                                            dialog.dismiss();

                                        }
                                    });

                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();

                                }

                                else
                                    Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();


                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {

                                Map<String, String> params = new HashMap<>();

                                params.put("nutritionid", String.valueOf(idList.get(position)));

                                return params;
                            }
                        };

                        Volley.newRequestQueue(context).add(request);

                    }
                });

                alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();

                alertDialog.show();

            }
        });

    }

    @Override
    public int getItemCount() {
        return idList.size();
    }

    class PostHolder extends RecyclerView.ViewHolder {

        ConstraintLayout container;
        TextView nameTextView;
        ImageView edit, delete;

        public PostHolder(@NonNull final View itemView) {
            super(itemView);

            container = itemView.findViewById(R.id.nutrition_container);
            nameTextView = itemView.findViewById(R.id.nutrition_title);
            edit = itemView.findViewById(R.id.editButton);
            delete = itemView.findViewById(R.id.deleteButton);

        }
    }
}