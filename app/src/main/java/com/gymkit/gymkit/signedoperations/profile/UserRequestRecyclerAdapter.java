package com.gymkit.gymkit.signedoperations.profile;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleDetailActivity;
import com.gymkit.gymkit.signedoperations.article.UpdateArticleActivity;
import com.squareup.picasso.Picasso;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class UserRequestRecyclerAdapter extends RecyclerView.Adapter<com.gymkit.gymkit.signedoperations.profile.UserRequestRecyclerAdapter.PostHolder> {

    private Context context;
    private ArrayList<String> idList;
    private ArrayList<String> fnameList;
    private ArrayList<String> lnameList;


    public UserRequestRecyclerAdapter(Context context, ArrayList<String> idList, ArrayList<String> fnameList, ArrayList<String> lnameList) {
        this.context = context;
        this.idList = idList;
        this.fnameList = fnameList;
        this.lnameList = lnameList;
    }

    @NonNull
    @Override
    public com.gymkit.gymkit.signedoperations.profile.UserRequestRecyclerAdapter.PostHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.request_recycler_row,parent,false);
        return new com.gymkit.gymkit.signedoperations.profile.UserRequestRecyclerAdapter.PostHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final com.gymkit.gymkit.signedoperations.profile.UserRequestRecyclerAdapter.PostHolder holder, final int position) {

        holder.tick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = "http://batuhanbatu.net/gymkitwebservice/confirm_user.php";

                StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response.contains("Successfully updated"))
                        {
                            idList.remove(position);
                            fnameList.remove(position);
                            lnameList.remove(position);

                            notifyDataSetChanged();

                            Toast.makeText(context, "User was successfully confirmed!", Toast.LENGTH_SHORT).show();
                        }

                        else
                        {
                            Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "Errorr!", Toast.LENGTH_LONG).show();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<>();

                        params.put("id", idList.get(position));

                        return params;
                    }
                };

                Volley.newRequestQueue(context).add(request);

            }
        });

        holder.cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = "http://batuhanbatu.net/gymkitwebservice/reject_user.php";

                StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response.contains("Successfully updated"))
                        {
                            idList.remove(position);
                            fnameList.remove(position);
                            lnameList.remove(position);

                            notifyDataSetChanged();

                            Toast.makeText(context, "User was successfully rejected!", Toast.LENGTH_SHORT).show();
                        }

                        else
                        {
                            Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "Errorr!", Toast.LENGTH_LONG).show();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<>();

                        params.put("id", idList.get(position));

                        return params;
                    }
                };

                Volley.newRequestQueue(context).add(request);

            }
        });

        holder.nameTextView.setText(fnameList.get(position) + " " + lnameList.get(position));

    }

    @Override
    public int getItemCount() {
        return idList.size();
    }

    class PostHolder extends RecyclerView.ViewHolder {

        ConstraintLayout container;
        TextView nameTextView;
        ImageView tick, cross;

        public PostHolder(@NonNull final View itemView) {
            super(itemView);

            container = itemView.findViewById(R.id.request_container);
            nameTextView = itemView.findViewById(R.id.nameRecycler);
            tick = itemView.findViewById(R.id.tick);
            cross = itemView.findViewById(R.id.cross);

        }
    }
}