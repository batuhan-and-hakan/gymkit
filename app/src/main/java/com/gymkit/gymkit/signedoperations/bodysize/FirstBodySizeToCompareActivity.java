package com.gymkit.gymkit.signedoperations.bodysize;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionDetailActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FirstBodySizeToCompareActivity extends AppCompatActivity {

    SharedPreferences session;
    FirstBodySizeRecyclerAdapter firstBodySizeRecyclerAdapter;
    ArrayList<String> idListFromDB;
    ArrayList<String> nameListFromDB;
    String userid = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_body_size_to_compare);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Select First Body Size");

        Intent getintent = getIntent();
        userid = getintent.getStringExtra("userid");

        idListFromDB = new ArrayList<>();
        nameListFromDB = new ArrayList<>();

        getBodySize();

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        firstBodySizeRecyclerAdapter = new FirstBodySizeRecyclerAdapter(FirstBodySizeToCompareActivity.this, userid, idListFromDB, nameListFromDB);
        recyclerView.setAdapter(firstBodySizeRecyclerAdapter);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(FirstBodySizeToCompareActivity.this);
        alertDialogBuilder.setTitle("IMPORTANT INFORMATION");
        alertDialogBuilder.setMessage("In this page, you'll choose 2 body size details to compare. After choosing first one, you'll be warned again!");
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void getBodySize()
    {
        idListFromDB.removeAll(idListFromDB);
        nameListFromDB.removeAll(nameListFromDB);

        String url = "http://batuhanbatu.net/gymkitwebservice/get_body_size_by_userid.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("body_size");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        idListFromDB.add(String.valueOf(row.getInt("id")));
                        nameListFromDB.add(row.getString("name"));
                    }

                    firstBodySizeRecyclerAdapter.notifyDataSetChanged();
                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("userid", userid);

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }
}