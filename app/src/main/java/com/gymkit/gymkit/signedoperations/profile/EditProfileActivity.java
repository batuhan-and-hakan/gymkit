package com.gymkit.gymkit.signedoperations.profile;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.article.WriteArticleActivity;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;
import com.gymkit.gymkit.unsignedoperations.MainActivity;
import com.gymkit.gymkit.unsignedoperations.SignUpActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class EditProfileActivity extends AppCompatActivity {

    SharedPreferences session;
    EditText fnameTextView, lnameTextView, emailTextView, phoneTextView, bdateTextView;
    RadioButton maleRadioButton, femaleRadioButton;
    Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Edit Profile");

        fnameTextView = findViewById(R.id.editText);
        lnameTextView = findViewById(R.id.editText2);
        emailTextView = findViewById(R.id.editText3);
        phoneTextView = findViewById(R.id.editText4);
        bdateTextView = findViewById(R.id.editText5);

        maleRadioButton = findViewById(R.id.maleRadioButton);
        femaleRadioButton = findViewById(R.id.femaleRadioButton);

        saveButton = findViewById(R.id.saveButton);

        getMyInfos();
    }

    public void getMyInfos()
    {
        String url = "http://batuhanbatu.net/gymkitwebservice/get_user_by_id.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("user");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        fnameTextView.setText(row.getString("fname"));
                        lnameTextView.setText(row.getString("lname"));
                        emailTextView.setText(row.getString("email"));
                        phoneTextView.setText(row.getString("phone"));

                        if (row.getString("gender").equals("1"))
                            maleRadioButton.setChecked(true);

                        else
                            femaleRadioButton.setChecked(true);

                        bdateTextView.setText(row.getString("bday"));

                    }

                }

                catch (JSONException e)
                {
                    e.printStackTrace();

                    Toast.makeText(EditProfileActivity.this, "Your login information(s) is wrong!", Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("id", String.valueOf(session.getInt("id", 0)));

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }

    public void pickDate(View view)
    {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month += 1;
                        bdateTextView.setText(year + "-" + month + "-" + dayOfMonth);
                    }
                }, year, month, dayOfMonth);

        dpd.setButton(DatePickerDialog.BUTTON_POSITIVE, "SELECT", dpd);
        dpd.setButton(DatePickerDialog.BUTTON_NEGATIVE, "CANCEL", dpd);
        dpd.show();
    }

    public void save(View view)
    {
        if (!maleRadioButton.isChecked() && !femaleRadioButton.isChecked())
            Toast.makeText(this, "All blanks must be filled!", Toast.LENGTH_LONG).show();

        else if (fnameTextView.getText().toString().equals("") || lnameTextView.getText().toString().equals("") || emailTextView.getText().toString().equals("") || phoneTextView.getText().toString().equals("") || bdateTextView.getText().toString().equals(""))
            Toast.makeText(this, "All blanks must be filled!", Toast.LENGTH_LONG).show();

        else if (!emailTextView.getText().toString().contains("@"))
            Toast.makeText(this, "Wrong email format!", Toast.LENGTH_LONG).show();

        else
        {
            saveButton.setEnabled(false);

            String url = "http://batuhanbatu.net/gymkitwebservice/update_profile.php";

            StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    if (response.contains("Successfully updated"))
                    {

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(EditProfileActivity.this);
                        alertDialogBuilder.setTitle("Updated Successfully");
                        alertDialogBuilder.setCancelable(false);
                        alertDialogBuilder.setMessage("Your profile was updated successfully! Now, you can go to your profile..");

                        alertDialogBuilder.setPositiveButton("Go To My Profile", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent intent = new Intent(EditProfileActivity.this, ProfileActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    }

                    else
                    {
                        Toast.makeText(EditProfileActivity.this, "Error!", Toast.LENGTH_LONG).show();
                        saveButton.setEnabled(true);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(EditProfileActivity.this, "Errorr!", Toast.LENGTH_LONG).show();
                    saveButton.setEnabled(true);
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();

                    params.put("id", String.valueOf(session.getInt("id", 0)));
                    params.put("fname", fnameTextView.getText().toString());
                    params.put("lname", lnameTextView.getText().toString());
                    params.put("email", emailTextView.getText().toString());
                    params.put("phone", phoneTextView.getText().toString());

                    if (maleRadioButton.isChecked())
                        params.put("gender", "1");
                    if (femaleRadioButton.isChecked())
                        params.put("gender", "0");

                    params.put("bday", bdateTextView.getText().toString());

                    return params;
                }
            };

            Volley.newRequestQueue(this).add(request);

        }
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(EditProfileActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(EditProfileActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(EditProfileActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(EditProfileActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(EditProfileActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}
