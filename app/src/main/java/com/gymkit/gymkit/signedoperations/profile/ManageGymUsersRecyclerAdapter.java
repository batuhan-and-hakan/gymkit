package com.gymkit.gymkit.signedoperations.profile;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.unsignedoperations.MainActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ManageGymUsersRecyclerAdapter extends RecyclerView.Adapter<com.gymkit.gymkit.signedoperations.profile.ManageGymUsersRecyclerAdapter.PostHolder> {

    private Context context;
    private ArrayList<String> idList;
    private ArrayList<String> fnameList;
    private ArrayList<String> lnameList;
    private ArrayList<String> userTypeList;


    public ManageGymUsersRecyclerAdapter(Context context, ArrayList<String> idList, ArrayList<String> fnameList, ArrayList<String> lnameList, ArrayList<String> userTypeList) {
        this.context = context;
        this.idList = idList;
        this.fnameList = fnameList;
        this.lnameList = lnameList;
        this.userTypeList = userTypeList;
    }

    @NonNull
    @Override
    public com.gymkit.gymkit.signedoperations.profile.ManageGymUsersRecyclerAdapter.PostHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.manage_user_recycler_row,parent,false);
        return new com.gymkit.gymkit.signedoperations.profile.ManageGymUsersRecyclerAdapter.PostHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final com.gymkit.gymkit.signedoperations.profile.ManageGymUsersRecyclerAdapter.PostHolder holder, final int position) {

        holder.nameTextView.setText(fnameList.get(position) + " " + lnameList.get(position));

        if (userTypeList.get(position).equals("1"))
            holder.changeUserType.setBackgroundResource(R.drawable.ic_trainer_face);

        else
            holder.changeUserType.setBackgroundResource(R.drawable.ic_user_face);


        holder.changeUserType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String changeUserTypeText = "";
                if (userTypeList.get(position).equals("1"))
                    changeUserTypeText = "Do you really want to make this trainer a normal user?";
                else
                    changeUserTypeText = "Do you really want to make this user a trainer?";


                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle("Are You Sure?");
                alertDialogBuilder.setMessage(changeUserTypeText);

                alertDialogBuilder.setCancelable(true);

                alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String url = "http://batuhanbatu.net/gymkitwebservice/update_user_type.php";

                        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                if (response.contains("Successfully updated"))
                                {
                                    if (userTypeList.get(position).equals("1"))
                                        userTypeList.set(position, "0");

                                    else
                                        userTypeList.set(position, "1");

                                    notifyDataSetChanged();

                                    Toast.makeText(context, "User type was successfully updated!", Toast.LENGTH_SHORT).show();
                                }

                                else
                                {
                                    Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();
                                }

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(context, "Errorr!", Toast.LENGTH_LONG).show();
                            }
                        }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {

                                Map<String, String> params = new HashMap<>();

                                params.put("id", idList.get(position));
                                params.put("usertype", userTypeList.get(position));

                                return params;
                            }
                        };

                        Volley.newRequestQueue(context).add(request);

                    }
                });

                alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();

                alertDialog.show();

            }
        });

        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle("Are You Sure?");
                alertDialogBuilder.setMessage("Do you really want to remove this user from your gym?");

                alertDialogBuilder.setCancelable(true);

                alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String url = "http://batuhanbatu.net/gymkitwebservice/delete_user_from_gym.php";

                        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                if (response.contains("Successfully updated"))
                                {
                                    idList.remove(position);
                                    fnameList.remove(position);
                                    lnameList.remove(position);
                                    userTypeList.remove(position);

                                    notifyDataSetChanged();

                                    Toast.makeText(context, "User was successfully removed from your gym!", Toast.LENGTH_SHORT).show();
                                }

                                else
                                {
                                    Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();
                                }

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(context, "Errorr!", Toast.LENGTH_LONG).show();
                            }
                        }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {

                                Map<String, String> params = new HashMap<>();

                                params.put("id", idList.get(position));

                                return params;
                            }
                        };

                        Volley.newRequestQueue(context).add(request);

                    }
                });

                alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();

                alertDialog.show();

            }
        });



    }

    @Override
    public int getItemCount() {
        return idList.size();
    }

    class PostHolder extends RecyclerView.ViewHolder {

        ConstraintLayout container;
        TextView nameTextView;
        ImageView changeUserType, remove;

        public PostHolder(@NonNull final View itemView) {
            super(itemView);

            container = itemView.findViewById(R.id.request_container);
            nameTextView = itemView.findViewById(R.id.nameRecycler);
            changeUserType = itemView.findViewById(R.id.changeUserType);
            remove = itemView.findViewById(R.id.remove);

        }
    }
}