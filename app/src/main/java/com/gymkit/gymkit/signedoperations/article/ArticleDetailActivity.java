package com.gymkit.gymkit.signedoperations.article;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;
import com.gymkit.gymkit.unsignedoperations.MainActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ArticleDetailActivity extends AppCompatActivity {

    Menu menu;

    SharedPreferences session;
    int articleid;
    ImageView photoImageView;
    TextView dateTextView, titleTextView, contentTextView, authorTextView;
    boolean amiowner = false;
    boolean didilike = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_detail);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Article Detail");

        Intent getintent = getIntent();
        articleid = Integer.valueOf(getintent.getStringExtra("articleid"));

        photoImageView = findViewById(R.id.photoImageView);
        dateTextView = findViewById(R.id.dateTextView);
        titleTextView = findViewById(R.id.titleTextView);
        contentTextView = findViewById(R.id.contentTextView);
        authorTextView = findViewById(R.id.authorTextView);

        getArticleDetails();

        getMyConnectionWithThisArticle();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        this.menu = menu;

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.articledetail_options_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.favorite)
        {
            if (!didilike)
            {
                didilike = true;
                item.setIcon(R.drawable.ic_favorite_isset);

                String url = "http://batuhanbatu.net/gymkitwebservice/add_article_favorite.php";

                StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response.contains("Successfully added"))
                            Toast.makeText(ArticleDetailActivity.this, "This article was successfully added to your favorite list!", Toast.LENGTH_LONG).show();


                        else
                            Toast.makeText(ArticleDetailActivity.this, "Error!", Toast.LENGTH_LONG).show();

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ArticleDetailActivity.this, "Errorr!", Toast.LENGTH_SHORT).show();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<>();

                        params.put("userid", String.valueOf(session.getInt("id", 0)));
                        params.put("articleid", String.valueOf(articleid));

                        return params;
                    }
                };

                Volley.newRequestQueue(ArticleDetailActivity.this).add(request);

            }
            else
            {
                didilike = false;
                item.setIcon(R.drawable.ic_favorite_empty);

                String url = "http://batuhanbatu.net/gymkitwebservice/delete_article_favorite.php";

                StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response.contains("Successfully deleted"))
                            Toast.makeText(ArticleDetailActivity.this, "This article was successfully deleted from your favorite list!", Toast.LENGTH_LONG).show();


                        else
                            Toast.makeText(ArticleDetailActivity.this, "Error!", Toast.LENGTH_LONG).show();

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ArticleDetailActivity.this, "Errorr!", Toast.LENGTH_SHORT).show();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<>();

                        params.put("userid", String.valueOf(session.getInt("id", 0)));
                        params.put("articleid", String.valueOf(articleid));

                        return params;
                    }
                };

                Volley.newRequestQueue(ArticleDetailActivity.this).add(request);

            }
        }

        else if (item.getItemId() == R.id.update)
        {
            Intent intent = new Intent(ArticleDetailActivity.this, UpdateArticleActivity.class);
            intent.putExtra("articleid", String.valueOf(articleid));
            startActivity(intent);
            finish();
        }

        else if (item.getItemId() == R.id.delete)
        {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ArticleDetailActivity.this);
            alertDialogBuilder.setTitle("Are You Sure?");
            alertDialogBuilder.setMessage("Do you really want to delete this article?");

            alertDialogBuilder.setCancelable(true);

            alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();

                    String url = "http://batuhanbatu.net/gymkitwebservice/delete_article.php";

                    StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            if (response.contains("Successfully deleted"))
                            {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ArticleDetailActivity.this);
                                alertDialogBuilder.setTitle("The article was deleted successfully!");
                                alertDialogBuilder.setCancelable(false);
                                alertDialogBuilder.setMessage("Now, you can go to the article list..");

                                alertDialogBuilder.setPositiveButton("Go To The List", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        Intent intent = new Intent(ArticleDetailActivity.this, ArticleListActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                            }

                            else
                                Toast.makeText(ArticleDetailActivity.this, "Error!", Toast.LENGTH_LONG).show();

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }){
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<>();

                            params.put("id", String.valueOf(articleid));

                            return params;
                        }
                    };

                    Volley.newRequestQueue(ArticleDetailActivity.this).add(request);

                }
            });

            alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();

                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();

            alertDialog.show();
        }

        return super.onOptionsItemSelected(item);
    }

    public void getMyConnectionWithThisArticle()
    {

        String url = "http://batuhanbatu.net/gymkitwebservice/get_connection_with_article.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                MenuItem favoriteItem = menu.findItem(R.id.favorite);

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("connection");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        if (row.getInt("owner") == 1)
                        {
                            amiowner = true;
                        }
                        else
                        {
                            amiowner = false;
                            menu.removeItem(R.id.update);
                            menu.removeItem(R.id.delete);
                        }

                        if (row.getInt("favorite") == 1)
                        {
                            didilike = true;
                            favoriteItem.setIcon(R.drawable.ic_favorite_isset);
                        }
                        else
                        {
                            didilike = false;
                            favoriteItem.setIcon(R.drawable.ic_favorite_empty);

                        }

                    }
                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("articleid", String.valueOf(articleid));
                params.put("userid", String.valueOf(session.getInt("id", 0)));

                return params;
            }
        };

        Volley.newRequestQueue(ArticleDetailActivity.this).add(request);

    }

    public void getArticleDetails()
    {

        String url = "http://batuhanbatu.net/gymkitwebservice/get_article_by_id.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("article");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        titleTextView.setText(row.getString("title").replace("~", "'"));
                        contentTextView.setText(row.getString("content").replace("~", "'"));
                        authorTextView.setText("By " + row.getString("authorname"));
                        dateTextView.setText(row.getString("date"));

                        Picasso.get().load(row.getString("photo")).into(photoImageView);

                    }
                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("id", String.valueOf(articleid));

                return params;
            }
        };

        Volley.newRequestQueue(ArticleDetailActivity.this).add(request);

    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(ArticleDetailActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(ArticleDetailActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(ArticleDetailActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(ArticleDetailActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(ArticleDetailActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}