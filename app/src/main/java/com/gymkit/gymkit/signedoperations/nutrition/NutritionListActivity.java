package com.gymkit.gymkit.signedoperations.nutrition;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.AddWorkoutActivity;
import com.gymkit.gymkit.signedoperations.workout.UserListToWorkoutActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NutritionListActivity extends AppCompatActivity {

    SharedPreferences session;
    boolean usertype = false;
    NutritionListRecyclerAdapter nutritionListRecyclerAdapter;
    ArrayList<String> idListFromDB;
    ArrayList<String> nameListFromDB;
    ArrayList<String> creatorIdListFromDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nutrition_list);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Nutrition List");

        idListFromDB = new ArrayList<>();
        nameListFromDB = new ArrayList<>();
        creatorIdListFromDB = new ArrayList<>();

        if (session.getString("usertype", "0").equals("1"))
        {
            usertype = true;
            getGymNutritions();
        }
        else
        {
            usertype = false;
            getMyNutritions();
        }

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        nutritionListRecyclerAdapter = new NutritionListRecyclerAdapter(NutritionListActivity.this, usertype, String.valueOf(session.getInt("id", 0)), idListFromDB, nameListFromDB, creatorIdListFromDB);
        recyclerView.setAdapter(nutritionListRecyclerAdapter);
    }

    public void getGymNutritions()
    {
        idListFromDB.removeAll(idListFromDB);
        nameListFromDB.removeAll(nameListFromDB);
        creatorIdListFromDB.removeAll(creatorIdListFromDB);

        String url = "http://batuhanbatu.net/gymkitwebservice/get_nutrition_as_trainer.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("nutrition");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        idListFromDB.add(String.valueOf(row.getInt("id")));
                        nameListFromDB.add(row.getString("name").replace("~", "'"));
                        creatorIdListFromDB.add(String.valueOf(row.getInt("creatorid")));
                    }

                    nutritionListRecyclerAdapter.notifyDataSetChanged();
                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("gymid", String.valueOf(session.getInt("gymid", 0)));

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);

    }

    public void getMyNutritions()
    {
        idListFromDB.removeAll(idListFromDB);
        nameListFromDB.removeAll(nameListFromDB);
        creatorIdListFromDB.removeAll(creatorIdListFromDB);

        String url = "http://batuhanbatu.net/gymkitwebservice/get_nutrition_as_user.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("nutrition");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        idListFromDB.add(String.valueOf(row.getInt("id")));
                        nameListFromDB.add(row.getString("name").replace("~", "'"));
                        creatorIdListFromDB.add(String.valueOf(row.getInt("creatorid")));
                    }

                    nutritionListRecyclerAdapter.notifyDataSetChanged();
                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("userid", String.valueOf(session.getInt("id", 0)));
                params.put("gymid", String.valueOf(session.getInt("gymid", 0)));

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.nutritionlist_options_menu, menu);

        if (!usertype)
        {
            menu.removeItem(R.id.user_list);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.add_nutrition) {

            Intent intent = new Intent(this, AddNutritionActivity.class);
            startActivity(intent);

        }

        else if (item.getItemId() == R.id.user_list)
        {
            Intent intent = new Intent(this, UserListToNutritionActivity.class);
            startActivity(intent);
        }

        else if (item.getItemId() == R.id.edit)
        {
            if (nutritionListRecyclerAdapter.editMode == true)
                nutritionListRecyclerAdapter.editMode = false;
            else
                nutritionListRecyclerAdapter.editMode = true;

            nutritionListRecyclerAdapter.notifyDataSetChanged();
        }

        return super.onOptionsItemSelected(item);
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(this, NutritionListActivity.class);
        startActivity(intent);
        finish();
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }
}
