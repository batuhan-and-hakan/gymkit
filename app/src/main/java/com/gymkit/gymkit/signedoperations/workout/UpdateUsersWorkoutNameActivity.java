package com.gymkit.gymkit.signedoperations.workout;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class UpdateUsersWorkoutNameActivity extends AppCompatActivity {

    SharedPreferences session;
    String id = "";
    String userid = "";
    EditText workoutName;
    Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_users_workout_name);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Update Users Workout Name");

        Intent getintent = getIntent();
        id = getintent.getStringExtra("id");
        userid = getintent.getStringExtra("userid");

        workoutName = findViewById(R.id.workout_name);
        saveButton = findViewById(R.id.saveButton);

        fillTheName();
    }

    public void save(View view)
    {
        if (workoutName.getText().toString().equals(""))
            Toast.makeText(this, "Workout Name Must Not Be Empty!", Toast.LENGTH_SHORT).show();

        else
        {
            saveButton.setEnabled(false);

            String url = "http://batuhanbatu.net/gymkitwebservice/update_workout_name.php";

            StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    if (response.contains("Successfully updated"))
                    {

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(UpdateUsersWorkoutNameActivity.this);
                        alertDialogBuilder.setTitle("Updated Successfully");
                        alertDialogBuilder.setCancelable(false);
                        alertDialogBuilder.setMessage("The workout was updated successfully! Now, you can go to the workout list..");

                        alertDialogBuilder.setPositiveButton("Go To The List", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent intent = new Intent(UpdateUsersWorkoutNameActivity.this, UsersWorkoutListActivity.class);
                                intent.putExtra("userid", userid);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    }

                    else
                    {
                        Toast.makeText(UpdateUsersWorkoutNameActivity.this, "Error!", Toast.LENGTH_LONG).show();
                        saveButton.setEnabled(true);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    saveButton.setEnabled(true);
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();

                    params.put("workoutid", String.valueOf(id));
                    params.put("name", workoutName.getText().toString().replace("'", "~"));

                    return params;
                }
            };

            Volley.newRequestQueue(this).add(request);
        }


    }

    public void fillTheName()
    {
        String url = "http://batuhanbatu.net/gymkitwebservice/get_workout_by_id.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("workout");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        workoutName.setText(row.getString("name").replace("~", "'"));
                    }

                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("id", id);

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(UpdateUsersWorkoutNameActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(UpdateUsersWorkoutNameActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(UpdateUsersWorkoutNameActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(UpdateUsersWorkoutNameActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(UpdateUsersWorkoutNameActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}