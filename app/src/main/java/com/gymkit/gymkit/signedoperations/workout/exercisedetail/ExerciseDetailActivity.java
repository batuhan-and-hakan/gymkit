package com.gymkit.gymkit.signedoperations.workout.exercisedetail;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ExerciseDetailActivity extends AppCompatActivity {

    SharedPreferences session;
    String id = "";
    String name = "";

    TextView exercise_name;
    WebView exercise_gif;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_detail);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Exercise Details");

        Intent getintent = getIntent();
        id = getintent.getStringExtra("id");



        exercise_name = findViewById(R.id.exercise_name);
        exercise_gif = findViewById(R.id.exercise_gif);

        try {
            name = getintent.getStringExtra("exercisename");
            exercise_name.setText(name);
        } catch (Exception e) {
            e.printStackTrace();
        }

        getExerciseDetails();

        exercise_gif.setOnTouchListener(new OnSwipeTouchListener(ExerciseDetailActivity.this) {
            public void onSwipeTop() {
            }
            public void onSwipeRight() {
            }
            public void onSwipeLeft() {

                Intent intent = new Intent(ExerciseDetailActivity.this, MuscleTabActivity.class);
                intent.putExtra("id", id);
                intent.putExtra("exercisename", exercise_name.getText().toString());
                startActivity(intent);
                finish();

            }
            public void onSwipeBottom() {
            }

        });
    }

    public void getExerciseDetails()
    {
        String url = "http://batuhanbatu.net/gymkitwebservice/get_exercise_by_id.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("exercise");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        if (exercise_name.getText().toString().equals(""))
                            exercise_name.setText(row.getString("name"));
                        exercise_gif.loadUrl("http://batuhanbatu.net/gymkitwebservice/gif_maker.php?url=" + row.getString("gif"));
                        exercise_gif.setBackgroundColor(Color.TRANSPARENT);
                    }

                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("id", id);

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }


    public void toExerciseDetail(View view)
    {
        /*Intent intent = new Intent(ExerciseDetailActivity.this, ExerciseDetailActivity.class);
        intent.putExtra("id", id);
        startActivity(intent);
        finish();*/
    }

    public void toMuscleTab(View view)
    {
        Intent intent = new Intent(ExerciseDetailActivity.this, MuscleTabActivity.class);
        intent.putExtra("id", id);
        intent.putExtra("exercisename", exercise_name.getText().toString());
        startActivity(intent);
        finish();
    }

    public void toHowTab(View view)
    {
        Intent intent = new Intent(ExerciseDetailActivity.this, HowTabActivity.class);
        intent.putExtra("id", id);
        intent.putExtra("exercisename", exercise_name.getText().toString());
        startActivity(intent);
        finish();
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(ExerciseDetailActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(ExerciseDetailActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(ExerciseDetailActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(ExerciseDetailActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(ExerciseDetailActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}