package com.gymkit.gymkit.signedoperations.nutrition;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.AddWorkoutToUserActivity;
import com.gymkit.gymkit.signedoperations.workout.UsersWorkoutListActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;

import java.util.HashMap;
import java.util.Map;

public class AddNutritionToUserActivity extends AppCompatActivity {

    SharedPreferences session;
    String userid = "";
    EditText nutritionName;
    Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_nutrition_to_user);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Add Nutrition To User");

        Intent getintent = getIntent();
        userid = getintent.getStringExtra("userid");

        nutritionName = findViewById(R.id.nutrition_name);
        saveButton = findViewById(R.id.saveButton);
    }

    public void save(View view)
    {
        if (nutritionName.getText().toString().equals(""))
            Toast.makeText(this, "Nutrition Name Must Not Be Empty!", Toast.LENGTH_SHORT).show();

        else
        {
            saveButton.setEnabled(false);

            String url = "http://batuhanbatu.net/gymkitwebservice/insert_nutrition.php";

            StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    if (response.contains("Successfully added"))
                    {

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AddNutritionToUserActivity.this);
                        alertDialogBuilder.setTitle("Added Successfully");
                        alertDialogBuilder.setCancelable(false);
                        alertDialogBuilder.setMessage("The nutrition was added successfully! Now, you can go to the nutrition list..");

                        alertDialogBuilder.setPositiveButton("Go To The List", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent intent = new Intent(AddNutritionToUserActivity.this, UsersNutritionListActivity.class);
                                intent.putExtra("userid", userid);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    }

                    else
                    {
                        Toast.makeText(AddNutritionToUserActivity.this, "Error!", Toast.LENGTH_LONG).show();
                        saveButton.setEnabled(true);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    saveButton.setEnabled(true);
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();

                    params.put("creatorid", String.valueOf(session.getInt("id", 0)));
                    params.put("name", nutritionName.getText().toString().replace("'", "~"));
                    params.put("gymid", String.valueOf(session.getInt("gymid", 0)));
                    params.put("userid", userid);


                    return params;
                }
            };

            Volley.newRequestQueue(this).add(request);
        }

    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(AddNutritionToUserActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(AddNutritionToUserActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(AddNutritionToUserActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(AddNutritionToUserActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(AddNutritionToUserActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}