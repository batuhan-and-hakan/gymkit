package com.gymkit.gymkit.signedoperations.bodysize;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.UpdateWorkoutNameActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutDetailActivity;
import com.gymkit.gymkit.unsignedoperations.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SecondBodySizeRecyclerAdapter extends RecyclerView.Adapter<com.gymkit.gymkit.signedoperations.bodysize.SecondBodySizeRecyclerAdapter.PostHolder> {

    private Context context;
    private String userid;
    private String firstid;
    private ArrayList<String> idList;
    private ArrayList<String> nameList;


    public SecondBodySizeRecyclerAdapter(Context context, String userid, String firstid, ArrayList<String> idList, ArrayList<String> nameList) {
        this.context = context;
        this.userid = userid;
        this.firstid = firstid;
        this.idList = idList;
        this.nameList = nameList;
    }

    @NonNull
    @Override
    public com.gymkit.gymkit.signedoperations.bodysize.SecondBodySizeRecyclerAdapter.PostHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.bodysize_list_recycler_row,parent,false);
        return new com.gymkit.gymkit.signedoperations.bodysize.SecondBodySizeRecyclerAdapter.PostHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final com.gymkit.gymkit.signedoperations.bodysize.SecondBodySizeRecyclerAdapter.PostHolder holder, final int position) {

        holder.nameTextView.setText(nameList.get(position));

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, CompareBodySizeActivity.class);
                intent.putExtra("firstid", firstid);
                intent.putExtra("secondid", idList.get(position));
                intent.putExtra("userid", userid);
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return idList.size();
    }

    class PostHolder extends RecyclerView.ViewHolder {

        ConstraintLayout container;
        TextView nameTextView;

        public PostHolder(@NonNull final View itemView) {
            super(itemView);

            container = itemView.findViewById(R.id.bodysize_container);
            nameTextView = itemView.findViewById(R.id.bodysize_title);

        }
    }
}