package com.gymkit.gymkit.signedoperations.nutrition;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UserListToAssignNutritionRecyclerAdapter extends RecyclerView.Adapter<com.gymkit.gymkit.signedoperations.nutrition.UserListToAssignNutritionRecyclerAdapter.PostHolder> {

    private Context context;
    private String nutritionid;
    private String creatorid;
    private String assignerid;
    private ArrayList<String> idList;
    private ArrayList<String> fnameList;
    private ArrayList<String> lnameList;


    public UserListToAssignNutritionRecyclerAdapter(Context context, String nutritionid, String creatorid, String assignerid, ArrayList<String> idList, ArrayList<String> fnameList, ArrayList<String> lnameList) {
        this.context = context;
        this.nutritionid = nutritionid;
        this.creatorid = creatorid;
        this.assignerid = assignerid;
        this.idList = idList;
        this.fnameList = fnameList;
        this.lnameList = lnameList;
    }

    @NonNull
    @Override
    public com.gymkit.gymkit.signedoperations.nutrition.UserListToAssignNutritionRecyclerAdapter.PostHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.user_list_recycler_row,parent,false);
        return new com.gymkit.gymkit.signedoperations.nutrition.UserListToAssignNutritionRecyclerAdapter.PostHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final com.gymkit.gymkit.signedoperations.nutrition.UserListToAssignNutritionRecyclerAdapter.PostHolder holder, final int position) {

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle("Are You Sure?");
                alertDialogBuilder.setMessage("Do you really want to assign this nutrition to this user?");

                alertDialogBuilder.setCancelable(true);

                alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                        String url = "http://batuhanbatu.net/gymkitwebservice/assign_nutrition.php";

                        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                if (response.contains("Successfully added"))
                                {
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                    alertDialogBuilder.setTitle("Successfull!");
                                    alertDialogBuilder.setCancelable(false);
                                    alertDialogBuilder.setMessage("The nutrition was successfully assigned to that user!");

                                    alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            Intent intent = new Intent(context, NutritionListActivity.class);
                                            context.startActivity(intent);

                                        }
                                    });

                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();

                                }

                                else
                                    Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();


                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(context, "Errorr!", Toast.LENGTH_LONG).show();

                            }
                        }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {

                                Map<String, String> params = new HashMap<>();

                                params.put("userid", String.valueOf(idList.get(position)));
                                params.put("nutritionid", nutritionid);
                                params.put("assignerid", assignerid);

                                return params;
                            }
                        };

                        Volley.newRequestQueue(context).add(request);

                    }
                });

                alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();

                alertDialog.show();

            }
        });

        holder.nameTextView.setText(fnameList.get(position) + " " + lnameList.get(position));

    }

    @Override
    public int getItemCount() {
        return idList.size();
    }

    class PostHolder extends RecyclerView.ViewHolder {

        ConstraintLayout container;
        TextView nameTextView;

        public PostHolder(@NonNull final View itemView) {
            super(itemView);

            container = itemView.findViewById(R.id.user_list_container);
            nameTextView = itemView.findViewById(R.id.nameRecycler);

        }
    }
}