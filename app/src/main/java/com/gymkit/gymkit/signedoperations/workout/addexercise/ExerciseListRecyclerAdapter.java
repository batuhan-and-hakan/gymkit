package com.gymkit.gymkit.signedoperations.workout.addexercise;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.workout.UsersWorkoutListActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ExerciseListRecyclerAdapter extends RecyclerView.Adapter<com.gymkit.gymkit.signedoperations.workout.addexercise.ExerciseListRecyclerAdapter.PostHolder> {

    private Context context;
    private String workoutid;
    private String creatorid;
    private ArrayList<String> idList;
    private ArrayList<String> nameList;
    private ArrayList<String> gifList;


    public ExerciseListRecyclerAdapter(Context context, String workoutid, String creatorid, ArrayList<String> idList, ArrayList<String> nameList, ArrayList<String> gifList) {
        this.context = context;
        this.workoutid = workoutid;
        this.creatorid = creatorid;
        this.idList = idList;
        this.nameList = nameList;
        this.gifList = gifList;
    }

    @NonNull
    @Override
    public com.gymkit.gymkit.signedoperations.workout.addexercise.ExerciseListRecyclerAdapter.PostHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.exerciselist_recycler_row,parent,false);
        return new com.gymkit.gymkit.signedoperations.workout.addexercise.ExerciseListRecyclerAdapter.PostHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final com.gymkit.gymkit.signedoperations.workout.addexercise.ExerciseListRecyclerAdapter.PostHolder holder, final int position) {

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, WorkoutCategoryListActivity.class);
                intent.putExtra("exerciseid", idList.get(position));
                intent.putExtra("workoutid", workoutid);
                intent.putExtra("creatorid", creatorid);
                context.startActivity(intent);
            }
        });

        holder.nameTextView.setText(nameList.get(position));

        holder.webView.loadUrl("http://batuhanbatu.net/gymkitwebservice/gif_maker.php?url=" + gifList.get(position));
        holder.webView.setBackgroundColor(Color.TRANSPARENT);

    }

    @Override
    public int getItemCount() {
        return idList.size();
    }

    class PostHolder extends RecyclerView.ViewHolder {

        ConstraintLayout container;
        TextView nameTextView;
        WebView webView;

        public PostHolder(@NonNull final View itemView) {
            super(itemView);

            container = itemView.findViewById(R.id.exercise_container);
            nameTextView = itemView.findViewById(R.id.exercise_name);
            webView = itemView.findViewById(R.id.exercise_gif);

        }
    }
}

