package com.gymkit.gymkit.signedoperations.article;

import android.content.Context;
import android.content.Intent;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ArticleListRecyclerAdapter extends RecyclerView.Adapter<ArticleListRecyclerAdapter.PostHolder> {

    private Context context;
    private long timeMilliSecond;
    private ArrayList<String> idList;
    private ArrayList<String> titleList;
    private ArrayList<String> photoList;
    private ArrayList<String> contentList;
    private ArrayList<String> dateList;


    public ArticleListRecyclerAdapter(Context context, long timeMilliSecond, ArrayList<String> idList, ArrayList<String> titleList, ArrayList<String> photoList, ArrayList<String> contentList, ArrayList<String> dateList) {
        this.context = context;
        this.timeMilliSecond = timeMilliSecond;
        this.idList = idList;
        this.titleList = titleList;
        this.photoList = photoList;
        this.contentList = contentList;
        this.dateList = dateList;
    }

    @NonNull
    @Override
    public PostHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.article_recycler_row,parent,false);
        return new PostHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PostHolder holder, final int position) {

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, ArticleDetailActivity.class);
                intent.putExtra("articleid", idList.get(position));
                context.startActivity(intent);

            }
        });

        SpannableString underlinedTitle = new SpannableString(titleList.get(position).replace("~", "'"));
        underlinedTitle.setSpan(new UnderlineSpan(), 0, underlinedTitle.length(), 0);
        holder.articleTitletextView.setText(underlinedTitle);

        if (contentList.get(position).length() > 81)
            holder.articleContenttextView.setText(contentList.get(position).substring(0, 80).replace("~", "'") + "...");

        else
            holder.articleContenttextView.setText(contentList.get(position).replace("~", "'"));


        String time = dateList.get(position);
        Date articleDate = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try
        {
            articleDate = sdf.parse(time);
        }
        catch (Exception e1)
        {
            e1.printStackTrace();
        }

        long articleTimeMilli = articleDate.getTime();

        long difference = timeMilliSecond - articleTimeMilli;

        if (TimeUnit.MILLISECONDS.toDays(difference) >= 365)
        {
            holder.articleDatetextView.setText(String.valueOf(new Timestamp(articleDate.getTime())));
        }

        else if (TimeUnit.MILLISECONDS.toDays(difference) >= 30)
        {
            if (TimeUnit.MILLISECONDS.toDays(difference) / 30 == 1)
                holder.articleDatetextView.setText("1 month ago");
            else
                holder.articleDatetextView.setText(String.valueOf(TimeUnit.MILLISECONDS.toDays(difference) / 30) + " months ago");
        }

        else if (TimeUnit.MILLISECONDS.toDays(difference) >= 7)
        {
            if (TimeUnit.MILLISECONDS.toDays(difference) / 7 == 1)
                holder.articleDatetextView.setText("1 week ago");
            else
                holder.articleDatetextView.setText(String.valueOf(TimeUnit.MILLISECONDS.toDays(difference) / 7) + " weeks ago");
        }

        else if (TimeUnit.MILLISECONDS.toDays(difference) >= 1)
        {
            if (TimeUnit.MILLISECONDS.toDays(difference) == 1)
                holder.articleDatetextView.setText("1 day ago");
            else
                holder.articleDatetextView.setText(String.valueOf(TimeUnit.MILLISECONDS.toDays(difference)) + " days ago");
        }

        else if (TimeUnit.MILLISECONDS.toHours(difference) >= 1)
        {
            if (TimeUnit.MILLISECONDS.toHours(difference) == 1)
                holder.articleDatetextView.setText("1 hour ago");
            else
                holder.articleDatetextView.setText(String.valueOf(TimeUnit.MILLISECONDS.toHours(difference)) + " hours ago");
        }

        else if (TimeUnit.MILLISECONDS.toMinutes(difference) >= 1)
        {
            if (TimeUnit.MILLISECONDS.toMinutes(difference) == 1)
                holder.articleDatetextView.setText("1 minute ago");
            else
                holder.articleDatetextView.setText(String.valueOf(TimeUnit.MILLISECONDS.toMinutes(difference)) + " minutes ago");
        }

        else if (TimeUnit.MILLISECONDS.toSeconds(difference) >= 1)
        {
            holder.articleDatetextView.setText(String.valueOf(TimeUnit.MILLISECONDS.toSeconds(difference)) + " second");
        }

        /*
        String differenceString = String.format("%d day, %d hour, %d min, %d sec",
                TimeUnit.MILLISECONDS.toDays(difference),
                TimeUnit.MILLISECONDS.toHours(difference) % 24,
                TimeUnit.MILLISECONDS.toMinutes(difference) % 60,
                TimeUnit.MILLISECONDS.toSeconds(difference) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(difference))
        );*/

        Picasso.get().load(photoList.get(position)).into(holder.articlePhotoimageView);

    }

    @Override
    public int getItemCount() {
        return idList.size();
    }

    class PostHolder extends RecyclerView.ViewHolder {

        ConstraintLayout container;
        TextView articleTitletextView;
        TextView articleContenttextView;
        TextView articleDatetextView;
        ImageView articlePhotoimageView;

        public PostHolder(@NonNull final View itemView) {
            super(itemView);

            container = itemView.findViewById(R.id.article_container);
            articleTitletextView = itemView.findViewById(R.id.article_title);
            articleContenttextView = itemView.findViewById(R.id.article_content);
            articleDatetextView = itemView.findViewById(R.id.article_date);
            articlePhotoimageView = itemView.findViewById(R.id.article_photo);

        }
    }
}
