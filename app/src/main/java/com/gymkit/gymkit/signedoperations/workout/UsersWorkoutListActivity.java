package com.gymkit.gymkit.signedoperations.workout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.profile.SavedArticleActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UsersWorkoutListActivity extends AppCompatActivity {

    SharedPreferences session;
    String userid;
    UsersWorkoutListRecyclerAdapter usersWorkoutListRecyclerAdapter;
    ArrayList<String> idListFromDB;
    ArrayList<String> nameListFromDB;
    ArrayList<String> creatorIdListFromDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_workout_list);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Users Workout List");

        Intent getintent = getIntent();
        userid = getintent.getStringExtra("userid");

        idListFromDB = new ArrayList<>();
        nameListFromDB = new ArrayList<>();
        creatorIdListFromDB = new ArrayList<>();

        getUsersWorkouts();

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        usersWorkoutListRecyclerAdapter = new UsersWorkoutListRecyclerAdapter(this, userid, idListFromDB, nameListFromDB, creatorIdListFromDB);
        recyclerView.setAdapter(usersWorkoutListRecyclerAdapter);
    }

    public void getUsersWorkouts()
    {
        idListFromDB.removeAll(idListFromDB);
        nameListFromDB.removeAll(nameListFromDB);

            String url = "http://batuhanbatu.net/gymkitwebservice/get_users_workout_list.php";

            StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try
                    {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray result = jsonObject.getJSONArray("workout");

                        for (int i = 0; i < result.length(); i++)
                        {
                            JSONObject row = result.getJSONObject(i);

                            idListFromDB.add(String.valueOf(row.getInt("id")));
                            nameListFromDB.add(row.getString("name").replace("~", "'"));
                            creatorIdListFromDB.add(row.getString("creatorid"));
                        }

                        usersWorkoutListRecyclerAdapter.notifyDataSetChanged();
                    }

                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();

                    params.put("userid", userid);
                    params.put("gymid", String.valueOf(session.getInt("gymid", 0)));

                    return params;
                }
            };

            Volley.newRequestQueue(this).add(request);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.usersworkoutlist_options_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.add_workout) {

            Intent intent = new Intent(this, AddWorkoutToUserActivity.class);
            intent.putExtra("userid", userid);
            startActivity(intent);

        }

        else if (item.getItemId() == R.id.edit)
        {
            if (usersWorkoutListRecyclerAdapter.editMode == true)
                usersWorkoutListRecyclerAdapter.editMode = false;
            else
                usersWorkoutListRecyclerAdapter.editMode = true;
            usersWorkoutListRecyclerAdapter.notifyDataSetChanged();
        }

        return super.onOptionsItemSelected(item);
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(UsersWorkoutListActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(UsersWorkoutListActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(UsersWorkoutListActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(UsersWorkoutListActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(UsersWorkoutListActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}