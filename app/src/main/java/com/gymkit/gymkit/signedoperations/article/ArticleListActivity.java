package com.gymkit.gymkit.signedoperations.article;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;
import com.gymkit.gymkit.unsignedoperations.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ArticleListActivity extends AppCompatActivity {

    SharedPreferences session;
    ArticleListRecyclerAdapter articleListRecyclerAdapter;
    ArrayList<String> idListFromDB;
    ArrayList<String> titleListFromDB;
    ArrayList<String> photoListFromDB;
    ArrayList<String> contentListFromDB;
    ArrayList<String> dateListFromDB;

    Date currentDate;
    long currentTimeMilli;
    Context context;

    String searchedText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_list);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Articles");

        idListFromDB = new ArrayList<>();
        titleListFromDB = new ArrayList<>();
        photoListFromDB = new ArrayList<>();
        contentListFromDB = new ArrayList<>();
        dateListFromDB = new ArrayList<>();

        getArticles();

        currentDate = new Date();
        currentTimeMilli = currentDate.getTime();
        context = ArticleListActivity.this;

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        articleListRecyclerAdapter = new ArticleListRecyclerAdapter(this, currentTimeMilli, idListFromDB, titleListFromDB, photoListFromDB, contentListFromDB, dateListFromDB);
        recyclerView.setAdapter(articleListRecyclerAdapter);

        final SwipeRefreshLayout pullToRefresh = findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentDate = new Date();
                currentTimeMilli = currentDate.getTime();

                RecyclerView recyclerView = findViewById(R.id.recyclerView);
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
                articleListRecyclerAdapter = new ArticleListRecyclerAdapter(context, currentTimeMilli, idListFromDB, titleListFromDB, photoListFromDB, contentListFromDB, dateListFromDB);
                recyclerView.setAdapter(articleListRecyclerAdapter);// your code
                pullToRefresh.setRefreshing(false);
            }
        });
    }

    public void getArticles()
    {

            idListFromDB.removeAll(idListFromDB);
            titleListFromDB.removeAll(titleListFromDB);
            photoListFromDB.removeAll(photoListFromDB);
            contentListFromDB.removeAll(contentListFromDB);
            dateListFromDB.removeAll(dateListFromDB);

            String url = "http://batuhanbatu.net/gymkitwebservice/get_all_reachable_articles.php";

            StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try
                    {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray result = jsonObject.getJSONArray("article");

                        for (int i = 0; i < result.length(); i++)
                        {
                            JSONObject row = result.getJSONObject(i);

                            idListFromDB.add(String.valueOf(row.getInt("id")));
                            titleListFromDB.add(row.getString("title").replace("~", "'"));
                            photoListFromDB.add(row.getString("photo"));
                            contentListFromDB.add(row.getString("content").replace("~", "'"));
                            dateListFromDB.add(row.getString("date"));

                        }

                        currentDate = new Date();
                        currentTimeMilli = currentDate.getTime();

                        articleListRecyclerAdapter.notifyDataSetChanged();
                    }

                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();

                    params.put("gymid", String.valueOf(session.getInt("gymid", 0)));

                    return params;
                }
            };

            Volley.newRequestQueue(this).add(request);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.articlelist_options_menu, menu);

        if (session.getString("usertype", "0").equals("0"))
        {
            menu.removeItem(R.id.write_an_article);
        }

        MenuItem searchItem = menu.findItem(R.id.app_bar_search);



        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                searchedText = query;

                idListFromDB.removeAll(idListFromDB);
                titleListFromDB.removeAll(titleListFromDB);
                photoListFromDB.removeAll(photoListFromDB);
                contentListFromDB.removeAll(contentListFromDB);
                dateListFromDB.removeAll(dateListFromDB);

                String url = "http://batuhanbatu.net/gymkitwebservice/search_in_reachable_articles.php";

                StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try
                        {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray result = jsonObject.getJSONArray("article");

                            for (int i = 0; i < result.length(); i++)
                            {
                                JSONObject row = result.getJSONObject(i);

                                idListFromDB.add(String.valueOf(row.getInt("id")));
                                titleListFromDB.add(row.getString("title").replace("~", "'"));
                                photoListFromDB.add(row.getString("photo"));
                                contentListFromDB.add(row.getString("content").replace("~", "'"));
                                dateListFromDB.add(row.getString("date"));

                            }

                            currentDate = new Date();
                            currentTimeMilli = currentDate.getTime();

                            articleListRecyclerAdapter.notifyDataSetChanged();
                        }

                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<>();

                        params.put("gymid", String.valueOf(session.getInt("gymid", 0)));
                        params.put("text", searchedText);

                        return params;
                    }
                };

                Volley.newRequestQueue(ArticleListActivity.this).add(request);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                searchedText = newText;

                idListFromDB.removeAll(idListFromDB);
                titleListFromDB.removeAll(titleListFromDB);
                photoListFromDB.removeAll(photoListFromDB);
                contentListFromDB.removeAll(contentListFromDB);
                dateListFromDB.removeAll(dateListFromDB);

                String url = "http://batuhanbatu.net/gymkitwebservice/search_in_reachable_articles.php";

                StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try
                        {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray result = jsonObject.getJSONArray("article");

                            for (int i = 0; i < result.length(); i++)
                            {
                                JSONObject row = result.getJSONObject(i);

                                idListFromDB.add(String.valueOf(row.getInt("id")));
                                titleListFromDB.add(row.getString("title").replace("~", "'"));
                                photoListFromDB.add(row.getString("photo"));
                                contentListFromDB.add(row.getString("content").replace("~", "'"));
                                dateListFromDB.add(row.getString("date"));

                            }

                            currentDate = new Date();
                            currentTimeMilli = currentDate.getTime();

                            articleListRecyclerAdapter.notifyDataSetChanged();
                        }

                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<>();

                        params.put("gymid", String.valueOf(session.getInt("gymid", 0)));
                        params.put("text", searchedText);

                        return params;
                    }
                };

                Volley.newRequestQueue(ArticleListActivity.this).add(request);

                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.write_an_article) {

            Intent intent = new Intent(ArticleListActivity.this, WriteArticleActivity.class);
            startActivity(intent);

        }

        return super.onOptionsItemSelected(item);
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(ArticleListActivity.this, ArticleListActivity.class);
        startActivity(intent);
        finish();
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(ArticleListActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(ArticleListActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(ArticleListActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(ArticleListActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}
