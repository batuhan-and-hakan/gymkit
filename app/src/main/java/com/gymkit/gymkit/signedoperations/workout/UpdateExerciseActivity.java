package com.gymkit.gymkit.signedoperations.workout;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class UpdateExerciseActivity extends AppCompatActivity {

    SharedPreferences session;
    String id = "";
    String workoutid = "";
    String creatorid = "";
    EditText set_num, rep_num;
    Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_exercise);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Update Exercise Details");

        Intent getintent = getIntent();
        id = getintent.getStringExtra("id");
        workoutid = getintent.getStringExtra("workoutid");
        creatorid = getintent.getStringExtra("creatorid");

        set_num = findViewById(R.id.set_num);
        rep_num = findViewById(R.id.rep_num);
        saveButton = findViewById(R.id.saveButton);

        fillTheBlanks();
    }

    public void save(View view)
    {
        if (set_num.getText().toString().equals("") || rep_num.getText().toString().equals(""))
            Toast.makeText(this, "Both Number Of Set And Repetition Must Not Be Empty!", Toast.LENGTH_LONG).show();

        else
        {
            saveButton.setEnabled(false);

            String url = "http://batuhanbatu.net/gymkitwebservice/update_workout_exercise_details.php";

            StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    if (response.contains("Successfully updated"))
                    {

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(UpdateExerciseActivity.this);
                        alertDialogBuilder.setTitle("Updated Successfully");
                        alertDialogBuilder.setCancelable(false);
                        alertDialogBuilder.setMessage("The exercise was updated successfully!");

                        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent intent = new Intent(UpdateExerciseActivity.this, WorkoutDetailActivity.class);
                                intent.putExtra("workoutid", workoutid);
                                intent.putExtra("creatorid", creatorid);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    }

                    else
                    {
                        Toast.makeText(UpdateExerciseActivity.this, "Error!", Toast.LENGTH_LONG).show();
                        saveButton.setEnabled(true);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    saveButton.setEnabled(true);
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();

                    params.put("id", String.valueOf(id));
                    params.put("setnum", set_num.getText().toString());
                    params.put("repetition", rep_num.getText().toString());

                    return params;
                }
            };

            Volley.newRequestQueue(this).add(request);
        }


    }

    public void fillTheBlanks()
    {
        String url = "http://batuhanbatu.net/gymkitwebservice/get_workout_exercise_by_id.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("exercise");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        set_num.setText(row.getString("setnum"));
                        rep_num.setText(row.getString("repetition"));
                    }

                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("id", id);

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(UpdateExerciseActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(UpdateExerciseActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(UpdateExerciseActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(UpdateExerciseActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(UpdateExerciseActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}