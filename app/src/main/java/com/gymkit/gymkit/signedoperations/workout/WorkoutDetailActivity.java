package com.gymkit.gymkit.signedoperations.workout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.addexercise.ExerciseGroupListActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WorkoutDetailActivity extends AppCompatActivity {

    SharedPreferences session;
    String workoutid = "";
    String creatorid = "";
    boolean usertype = false;
    WorkoutExerciseListRecyclerAdapter workoutExerciseListRecyclerAdapter;
    ArrayList<String> dataTypeListFromDB;
    ArrayList<String> idListFromDB;
    ArrayList<String> weidListFromDB;
    ArrayList<String> nameListFromDB;
    ArrayList<String> gifListFromDB;
    ArrayList<String> setListFromDB;
    ArrayList<String> repListFromDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout_detail);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Workout Detail");

        dataTypeListFromDB = new ArrayList<>();
        idListFromDB = new ArrayList<>();
        weidListFromDB = new ArrayList<>();
        nameListFromDB = new ArrayList<>();
        gifListFromDB = new ArrayList<>();
        setListFromDB = new ArrayList<>();
        repListFromDB = new ArrayList<>();

        if (session.getString("usertype", "0").equals("1"))
            usertype = true;
        else
            usertype = false;

        Intent getintent = getIntent();
        workoutid = getintent.getStringExtra("workoutid");
        creatorid = getintent.getStringExtra("creatorid");

        getWorkoutDetails();

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        workoutExerciseListRecyclerAdapter = new WorkoutExerciseListRecyclerAdapter(this, usertype, String.valueOf(session.getInt("id", 0)), creatorid, workoutid, dataTypeListFromDB, idListFromDB, weidListFromDB, nameListFromDB, gifListFromDB, setListFromDB, repListFromDB);
        recyclerView.setAdapter(workoutExerciseListRecyclerAdapter);
    }

    public void getWorkoutDetails()
    {
        dataTypeListFromDB.removeAll(dataTypeListFromDB);
        idListFromDB.removeAll(idListFromDB);
        weidListFromDB.removeAll(weidListFromDB);
        nameListFromDB.removeAll(nameListFromDB);
        gifListFromDB.removeAll(gifListFromDB);
        setListFromDB.removeAll(setListFromDB);
        repListFromDB.removeAll(repListFromDB);

        String url = "http://batuhanbatu.net/gymkitwebservice/get_workout_details.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("detail");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        //weid = String.valueOf(row.getInt("weid"));

                        if (String.valueOf(row.getInt("type")).equals("0"))
                        {
                            dataTypeListFromDB.add("0");
                            idListFromDB.add(String.valueOf(row.getInt("id")));
                            weidListFromDB.add("");
                            nameListFromDB.add(row.getString("name").replace("~", "'"));
                            gifListFromDB.add("");
                            setListFromDB.add("");
                            repListFromDB.add("");
                        }
                        else
                        {
                            dataTypeListFromDB.add("1");
                            idListFromDB.add(String.valueOf(row.getInt("id")));
                            weidListFromDB.add(String.valueOf(row.getInt("weid")));
                            nameListFromDB.add(row.getString("name").replace("~", "'"));
                            gifListFromDB.add(row.getString("gif"));
                            setListFromDB.add(row.getString("setnum"));
                            repListFromDB.add(row.getString("repetition"));
                        }

                    }

                    workoutExerciseListRecyclerAdapter.notifyDataSetChanged();
                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("workoutid", workoutid);

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.workoutdetail_options_menu, menu);

        if (!usertype && !String.valueOf(session.getInt("id", 0)).equals(creatorid))
        {
            menu.removeItem(R.id.add_exercise);
            menu.removeItem(R.id.add_category);
            menu.removeItem(R.id.edit);
        }

        if (!usertype)
        {
            menu.removeItem(R.id.assign_to_user);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.assign_to_user) {

            Intent intent = new Intent(WorkoutDetailActivity.this, AssignWorkoutActivity.class);
            intent.putExtra("workoutid", workoutid);
            intent.putExtra("creatorid", creatorid);
            startActivity(intent);
        }

        else if (item.getItemId() == R.id.add_exercise) {

            Intent intent = new Intent(WorkoutDetailActivity.this, ExerciseGroupListActivity.class);
            intent.putExtra("workoutid", workoutid);
            intent.putExtra("creatorid", creatorid);
            startActivity(intent);

        }

        else if (item.getItemId() == R.id.add_category)
        {
            Intent intent = new Intent(WorkoutDetailActivity.this, AddWorkoutCategoryActivity.class);
            intent.putExtra("workoutid", workoutid);
            intent.putExtra("creatorid", creatorid);
            startActivity(intent);
        }

        else if (item.getItemId() == R.id.edit)
        {
            if (workoutExerciseListRecyclerAdapter.editMode == true)
                workoutExerciseListRecyclerAdapter.editMode = false;
            else
                workoutExerciseListRecyclerAdapter.editMode = true;

            workoutExerciseListRecyclerAdapter.notifyDataSetChanged();
        }

        return super.onOptionsItemSelected(item);
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(WorkoutDetailActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(WorkoutDetailActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(WorkoutDetailActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(WorkoutDetailActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(WorkoutDetailActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}