package com.gymkit.gymkit.signedoperations.workout.addexercise;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WorkoutCategoryListActivity extends AppCompatActivity {

    SharedPreferences session;
    WorkoutCategoryListRecyclerAdapter workoutCategoryListRecyclerAdapter;
    ArrayList<String> idListFromDB;
    ArrayList<String> nameListFromDB;

    String exerciseid = "";
    String workoutid = "";
    String creatorid = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout_category_list);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Category List");

        Intent getintent = getIntent();
        exerciseid = getintent.getStringExtra("exerciseid");
        workoutid = getintent.getStringExtra("workoutid");
        creatorid = getintent.getStringExtra("creatorid");

        idListFromDB = new ArrayList<>();
        nameListFromDB = new ArrayList<>();

        getCategoryList();

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        workoutCategoryListRecyclerAdapter = new WorkoutCategoryListRecyclerAdapter(this, exerciseid, workoutid, creatorid, idListFromDB, nameListFromDB);
        recyclerView.setAdapter(workoutCategoryListRecyclerAdapter);
    }

    public void getCategoryList()
    {
        idListFromDB.removeAll(idListFromDB);
        nameListFromDB.removeAll(nameListFromDB);

        String url = "http://batuhanbatu.net/gymkitwebservice/get_workout_category_by_workoutid.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("category");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        idListFromDB.add(String.valueOf(row.getInt("id")));
                        nameListFromDB.add(row.getString("name").replace("~", "'"));

                    }

                    workoutCategoryListRecyclerAdapter.notifyDataSetChanged();

                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(WorkoutCategoryListActivity.this, "Error!", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("workoutid", workoutid);

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(WorkoutCategoryListActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(WorkoutCategoryListActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(WorkoutCategoryListActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(WorkoutCategoryListActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(WorkoutCategoryListActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}