package com.gymkit.gymkit.signedoperations.bodysize;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CompareBodySizeActivity extends AppCompatActivity {

    SharedPreferences session;
    String firstid = "";
    String secondid = "";
    String userid = "";
    TextView name, weight1, weight2, fluid1, fluid2, oil1, oil2, muscle1, muscle2;
    ImageView weightIcon, fluidIcon, oilIcon, muscleIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compare_body_size);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Compare Body Size");

        Intent getintent = getIntent();
        firstid = getintent.getStringExtra("firstid");
        secondid = getintent.getStringExtra("secondid");
        userid = getintent.getStringExtra("userid");

        name = findViewById(R.id.name);
        weight1 = findViewById(R.id.weight1);
        weight2 = findViewById(R.id.weight2);
        fluid1 = findViewById(R.id.fluid1);
        fluid2 = findViewById(R.id.fluid2);
        oil1 = findViewById(R.id.oil1);
        oil2 = findViewById(R.id.oil2);
        muscle1 = findViewById(R.id.muscle1);
        muscle2 = findViewById(R.id.muscle2);
        weightIcon = findViewById(R.id.weightIcon);
        fluidIcon = findViewById(R.id.fluidIcon);
        oilIcon = findViewById(R.id.oilIcon);
        muscleIcon = findViewById(R.id.muscleIcon);

        fillThePage();
    }

    public void fillThePage()
    {
        String url = "http://batuhanbatu.net/gymkitwebservice/compare_body_size.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("body_size");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        name.setText(row.getString("name1") + " vs " + row.getString("name2"));
                        weight1.setText(row.getString("weight1") + " kg");
                        weight2.setText(row.getString("weight2") + " kg");
                        fluid1.setText(row.getString("fluid1") + " %");
                        fluid2.setText(row.getString("fluid2") + " %");
                        oil1.setText(row.getString("oil1") + " %");
                        oil2.setText(row.getString("oil2") + " %");
                        muscle1.setText(row.getString("muscle1") + " %");
                        muscle2.setText(row.getString("muscle2") + " %");

                        switch (row.getString("weightt"))
                        {
                            case "-1":
                                weightIcon.setImageResource(R.drawable.ic_arrow_down);
                                break;
                            case "0":
                                weightIcon.setImageResource(R.drawable.ic_remove);
                                break;
                            case "1":
                                weightIcon.setImageResource(R.drawable.ic_arrow_up);
                                break;
                        }

                        switch (row.getString("fluidd"))
                        {
                            case "-1":
                                fluidIcon.setImageResource(R.drawable.ic_arrow_down);
                                break;
                            case "0":
                                fluidIcon.setImageResource(R.drawable.ic_remove);
                                break;
                            case "1":
                                fluidIcon.setImageResource(R.drawable.ic_arrow_up);
                                break;
                        }

                        switch (row.getString("oill"))
                        {
                            case "-1":
                                oilIcon.setImageResource(R.drawable.ic_arrow_down);
                                break;
                            case "0":
                                oilIcon.setImageResource(R.drawable.ic_remove);
                                break;
                            case "1":
                                oilIcon.setImageResource(R.drawable.ic_arrow_up);
                                break;
                        }

                        switch (row.getString("musclee"))
                        {
                            case "-1":
                                muscleIcon.setImageResource(R.drawable.ic_arrow_down);
                                break;
                            case "0":
                                muscleIcon.setImageResource(R.drawable.ic_remove);
                                break;
                            case "1":
                                muscleIcon.setImageResource(R.drawable.ic_arrow_up);
                                break;
                        }
                    }

                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("firstid", firstid);
                params.put("secondid", secondid);

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }
}