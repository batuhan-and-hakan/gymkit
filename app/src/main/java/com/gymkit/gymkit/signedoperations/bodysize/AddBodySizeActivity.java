package com.gymkit.gymkit.signedoperations.bodysize;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;

import java.util.HashMap;
import java.util.Map;

public class AddBodySizeActivity extends AppCompatActivity {

    SharedPreferences session;
    String userid = "";
    EditText name, weight, fluid, oil, muscle;
    Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_body_size);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Add Body Size");

        Intent getintent = getIntent();
        userid = getintent.getStringExtra("userid");

        name = findViewById(R.id.name);
        weight = findViewById(R.id.weight);
        fluid = findViewById(R.id.fluid);
        oil = findViewById(R.id.oil);
        muscle = findViewById(R.id.muscle);
        saveButton = findViewById(R.id.saveButton);
    }

    public void save(View view)
    {
        if (name.getText().toString().equals("") || weight.getText().toString().equals("") || fluid.getText().toString().equals("") || oil.getText().toString().equals("") || muscle.getText().toString().equals(""))
            Toast.makeText(this, "All Fields Must Not Be Empty!", Toast.LENGTH_SHORT).show();

        else if (Double.parseDouble(weight.getText().toString()) < 0 || Double.parseDouble(weight.getText().toString()) > 250 || Double.parseDouble(fluid.getText().toString()) < 0 || Double.parseDouble(fluid.getText().toString()) > 100 || Double.parseDouble(oil.getText().toString()) < 0 || Double.parseDouble(oil.getText().toString()) > 100 || Double.parseDouble(muscle.getText().toString()) < 0 || Double.parseDouble(muscle.getText().toString()) > 100)
            Toast.makeText(this, "Fields Must Be Filled By Valid Numbers!", Toast.LENGTH_SHORT).show();

        else
        {
            saveButton.setEnabled(false);

            String url = "http://batuhanbatu.net/gymkitwebservice/add_body_size.php";

            StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    if (response.contains("Successfully added"))
                    {

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AddBodySizeActivity.this);
                        alertDialogBuilder.setTitle("Added Successfully");
                        alertDialogBuilder.setCancelable(false);
                        alertDialogBuilder.setMessage("The body size was added successfully!");

                        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent intent = new Intent(AddBodySizeActivity.this, UsersBodySizeListActivity.class);
                                intent.putExtra("userid", userid);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    }

                    else
                    {
                        Toast.makeText(AddBodySizeActivity.this, "Error!", Toast.LENGTH_LONG).show();
                        saveButton.setEnabled(true);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    saveButton.setEnabled(true);
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();

                    params.put("name", name.getText().toString());
                    params.put("weight", weight.getText().toString());
                    params.put("fluid", fluid.getText().toString());
                    params.put("oil", oil.getText().toString());
                    params.put("muscle", muscle.getText().toString());
                    params.put("userid", String.valueOf(userid));
                    params.put("creatorid", String.valueOf(session.getInt("id", 0)));

                    return params;
                }
            };

            Volley.newRequestQueue(this).add(request);
        }


    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }
}