package com.gymkit.gymkit.signedoperations.nutrition;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.AssignWorkoutActivity;
import com.gymkit.gymkit.signedoperations.workout.UserListToAssignRecyclerAdapter;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AssignNutritionActivity extends AppCompatActivity {

    SharedPreferences session;
    String nutritionid = "";
    String creatorid = "";
    UserListToAssignNutritionRecyclerAdapter userListToAssignNutritionRecyclerAdapter;
    ArrayList<String> idListFromDB;
    ArrayList<String> fnameListFromDB;
    ArrayList<String> lnameListFromDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_nutrition);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("User List");

        idListFromDB = new ArrayList<>();
        fnameListFromDB = new ArrayList<>();
        lnameListFromDB = new ArrayList<>();

        Intent getintent = getIntent();
        nutritionid = getintent.getStringExtra("nutritionid");
        creatorid = getintent.getStringExtra("creatorid");

        getUserList();

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        userListToAssignNutritionRecyclerAdapter = new UserListToAssignNutritionRecyclerAdapter(this, nutritionid, creatorid, String.valueOf(session.getInt("id", 0)), idListFromDB, fnameListFromDB, lnameListFromDB);
        recyclerView.setAdapter(userListToAssignNutritionRecyclerAdapter);
    }

    public void getUserList()
    {
        idListFromDB.removeAll(idListFromDB);
        fnameListFromDB.removeAll(fnameListFromDB);
        lnameListFromDB.removeAll(lnameListFromDB);

        String url = "http://batuhanbatu.net/gymkitwebservice/get_user_list.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("user");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        idListFromDB.add(String.valueOf(row.getInt("id")));
                        fnameListFromDB.add(row.getString("fname"));
                        lnameListFromDB.add(row.getString("lname"));

                    }

                    userListToAssignNutritionRecyclerAdapter.notifyDataSetChanged();

                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                    Toast.makeText(AssignNutritionActivity.this, "Error!", Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(AssignNutritionActivity.this, "Error!", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("gymid", String.valueOf(session.getInt("gymid", 0)));

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(AssignNutritionActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(AssignNutritionActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(AssignNutritionActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(AssignNutritionActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(AssignNutritionActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}