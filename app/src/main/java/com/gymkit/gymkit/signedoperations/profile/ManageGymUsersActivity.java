package com.gymkit.gymkit.signedoperations.profile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ManageGymUsersActivity extends AppCompatActivity {

    SharedPreferences session;
    ManageGymUsersRecyclerAdapter manageGymUsersRecyclerAdapter;
    ArrayList<String> idListFromDB;
    ArrayList<String> fnameListFromDB;
    ArrayList<String> lnameListFromDB;
    ArrayList<String> userTypeListFromDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_gym_users);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Manage Gym Users");

        idListFromDB = new ArrayList<>();
        fnameListFromDB = new ArrayList<>();
        lnameListFromDB = new ArrayList<>();
        userTypeListFromDB = new ArrayList<>();

        getUsers();

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        manageGymUsersRecyclerAdapter = new ManageGymUsersRecyclerAdapter(this, idListFromDB, fnameListFromDB, lnameListFromDB, userTypeListFromDB);
        recyclerView.setAdapter(manageGymUsersRecyclerAdapter);
    }

    public void getUsers()
    {

        idListFromDB.removeAll(idListFromDB);
        fnameListFromDB.removeAll(fnameListFromDB);
        lnameListFromDB.removeAll(lnameListFromDB);
        userTypeListFromDB.removeAll(userTypeListFromDB);

        String url = "http://batuhanbatu.net/gymkitwebservice/get_gym_users.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("user");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        idListFromDB.add(String.valueOf(row.getInt("id")));
                        fnameListFromDB.add(row.getString("fname"));
                        lnameListFromDB.add(row.getString("lname"));
                        userTypeListFromDB.add(String.valueOf(row.getInt("usertype")));

                    }

                    manageGymUsersRecyclerAdapter.notifyDataSetChanged();
                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("gymid", String.valueOf(session.getInt("gymid", 0)));
                params.put("userid", String.valueOf(session.getInt("id", 0)));

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);

    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(ManageGymUsersActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(ManageGymUsersActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(ManageGymUsersActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(ManageGymUsersActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(ManageGymUsersActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}