package com.gymkit.gymkit.signedoperations.profile;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.article.UpdateArticleActivity;
import com.gymkit.gymkit.signedoperations.article.WriteArticleActivity;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ChangePasswordActivity extends AppCompatActivity {

    SharedPreferences session;
    TextView oldPasswordTextView, newPasswordTextView, newPassword2TextView;
    Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Change Password");

        oldPasswordTextView = findViewById(R.id.oldPassword);
        newPasswordTextView = findViewById(R.id.newPassword);
        newPassword2TextView = findViewById(R.id.newPassword2);
        saveButton = findViewById(R.id.saveButton);
    }

    public void save(View view)
    {
        if (oldPasswordTextView.getText().toString().equals("") || newPasswordTextView.getText().toString().equals("") || newPassword2TextView.getText().toString().equals(""))
            Toast.makeText(this, "Passwords can not be empty!", Toast.LENGTH_LONG).show();

        else if (!newPasswordTextView.getText().toString().equals(newPassword2TextView.getText().toString()))
            Toast.makeText(this, "New passwords should be same!", Toast.LENGTH_LONG).show();

        else
        {
            saveButton.setEnabled(false);

            String url = "http://batuhanbatu.net/gymkitwebservice/update_signed_password.php";

            StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    if (response.contains("Successfully updated"))
                    {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ChangePasswordActivity.this);
                        alertDialogBuilder.setTitle("Your password was changed successfully!");
                        alertDialogBuilder.setCancelable(false);
                        alertDialogBuilder.setMessage("Now, you can go to your profile..");

                        alertDialogBuilder.setPositiveButton("Go To My Profile", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent intent = new Intent(ChangePasswordActivity.this, ProfileActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }

                    else
                    {
                        Toast.makeText(ChangePasswordActivity.this, "Old password is wrong!", Toast.LENGTH_LONG).show();
                        saveButton.setEnabled(true);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(ChangePasswordActivity.this, "Errorr!", Toast.LENGTH_LONG).show();
                    saveButton.setEnabled(true);
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();

                    params.put("userid", String.valueOf(session.getInt("id", 0)));
                    params.put("oldpassword", oldPasswordTextView.getText().toString());
                    params.put("newpassword", newPasswordTextView.getText().toString());

                    return params;
                }
            };

            Volley.newRequestQueue(this).add(request);

        }
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(ChangePasswordActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(ChangePasswordActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(ChangePasswordActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(ChangePasswordActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(ChangePasswordActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}