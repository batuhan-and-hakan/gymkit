package com.gymkit.gymkit.signedoperations.nutrition;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.workout.UpdateExerciseActivity;
import com.gymkit.gymkit.signedoperations.workout.UpdateWorkoutCategoryActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutDetailActivity;
import com.gymkit.gymkit.signedoperations.workout.exercisedetail.ExerciseDetailActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NutritionDetailListRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public boolean editMode = false;

    private Context context;
    public boolean usertype = false;
    private String userid;
    private String creatorid;
    private String nutritionid;
    private ArrayList<String> dataTypeList;
    private ArrayList<String> idList;
    private ArrayList<String> nameList;
    private ArrayList<String> contentList;


    public NutritionDetailListRecyclerAdapter(Context context, boolean usertype, String userid, String creatorid, String nutritionid, ArrayList<String> dataTypeList, ArrayList<String> idList, ArrayList<String> nameList, ArrayList<String> contentList) {
        this.context = context;
        this.usertype = usertype;
        this.userid = userid;
        this.creatorid = creatorid;
        this.nutritionid = nutritionid;
        this.dataTypeList = dataTypeList;
        this.idList = idList;
        this.nameList = nameList;
        this.contentList = contentList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        switch(viewType){
            case 0: //category
            {
                View listItem = layoutInflater.inflate(R.layout.nutrition_category_recycler_row, parent, false);
                RecyclerView.ViewHolder viewHolder = new com.gymkit.gymkit.signedoperations.nutrition.NutritionDetailListRecyclerAdapter.CategoryViewHolder(listItem);
                return viewHolder;
            }
            case 1: //meal
            {
                View listItem = layoutInflater.inflate(R.layout.nutrition_meal_recycler_row, parent, false);
                RecyclerView.ViewHolder viewHolder = new com.gymkit.gymkit.signedoperations.nutrition.NutritionDetailListRecyclerAdapter.MealViewHolder(listItem);
                return viewHolder;
            }
        }

        return null;

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        switch (holder.getItemViewType()) {

            case 0: //category
                com.gymkit.gymkit.signedoperations.nutrition.NutritionDetailListRecyclerAdapter.CategoryViewHolder category = (com.gymkit.gymkit.signedoperations.nutrition.NutritionDetailListRecyclerAdapter.CategoryViewHolder) holder;

                category.textView.setText(nameList.get(position));


                if (editMode && (usertype || userid.equals(creatorid)))
                {
                    category.editButton.setVisibility(View.VISIBLE);
                    category.deleteButton.setVisibility(View.VISIBLE);
                }

                else
                {
                    category.editButton.setVisibility(View.GONE);
                    category.deleteButton.setVisibility(View.GONE);
                }

                category.editButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, UpdateNutritionCategoryActivity.class);
                        intent.putExtra("id", idList.get(position));
                        intent.putExtra("nutritionid", nutritionid);
                        intent.putExtra("creatorid", creatorid);
                        context.startActivity(intent);

                    }
                });

                category.deleteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                        alertDialogBuilder.setTitle("Are You Sure?");
                        alertDialogBuilder.setMessage("Do you really want to delete this category?");

                        alertDialogBuilder.setCancelable(true);

                        alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();

                                String url = "http://batuhanbatu.net/gymkitwebservice/delete_nutrition_category.php";

                                StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {

                                        if (response.contains("Successfully deleted"))
                                        {

                                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                            alertDialogBuilder.setTitle("Successfull!");
                                            alertDialogBuilder.setCancelable(false);
                                            alertDialogBuilder.setMessage("The category was successfully deleted!");

                                            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                    Intent intent = new Intent(context, NutritionDetailActivity.class);
                                                    intent.putExtra("nutritionid", nutritionid);
                                                    intent.putExtra("creatorid", creatorid);
                                                    context.startActivity(intent);

                                                }
                                            });

                                            AlertDialog alertDialog = alertDialogBuilder.create();
                                            alertDialog.show();

                                        }

                                        else
                                            Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();


                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {

                                    }
                                }){
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {

                                        Map<String, String> params = new HashMap<>();

                                        params.put("id", String.valueOf(idList.get(position)));

                                        return params;
                                    }
                                };

                                Volley.newRequestQueue(context).add(request);

                            }
                        });

                        alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();

                        alertDialog.show();

                    }
                });

                break;

            case 1: //meal
                com.gymkit.gymkit.signedoperations.nutrition.NutritionDetailListRecyclerAdapter.MealViewHolder meal = (com.gymkit.gymkit.signedoperations.nutrition.NutritionDetailListRecyclerAdapter.MealViewHolder) holder;

                meal.meal_title.setText(nameList.get(position));

                meal.meal_container.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, MealDetailActivity.class);
                        intent.putExtra("id", idList.get(position));
                        intent.putExtra("nutritionid", nutritionid);
                        intent.putExtra("creatorid", creatorid);
                        context.startActivity(intent);

                    }
                });

                break;
        }

    }

    @Override
    public int getItemViewType(int position) {
        return Integer.parseInt(dataTypeList.get(position));
    }

    @Override
    public int getItemCount() {
        return idList.size();
    }

    public static class CategoryViewHolder extends RecyclerView.ViewHolder {

        public TextView textView;
        public ImageView editButton, deleteButton;

        public CategoryViewHolder(@NonNull View itemView) {

            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.textView);
            editButton = (ImageView) itemView.findViewById(R.id.editButton);
            deleteButton = (ImageView) itemView.findViewById(R.id.deleteButton);

        }
    }
    public static class MealViewHolder extends RecyclerView.ViewHolder {

        public ConstraintLayout meal_container;
        public TextView meal_title;
        //public ImageView editButton, deleteButton;


        public MealViewHolder(@NonNull View itemView) {

            super(itemView);
            meal_container = (ConstraintLayout) itemView.findViewById(R.id.meal_container);
            meal_title = (TextView) itemView.findViewById(R.id.meal_title);
            /*editButton = (ImageView) itemView.findViewById(R.id.editButton);
            deleteButton = (ImageView) itemView.findViewById(R.id.deleteButton);*/

        }

    }

}