package com.gymkit.gymkit.signedoperations.nutrition;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;

import java.util.HashMap;
import java.util.Map;

public class AddMealActivity extends AppCompatActivity {

    SharedPreferences session;
    String categoryid = "";
    String nutritionid = "";
    String creatorid = "";

    EditText meal_name, meal_content;
    Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_meal);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Add Meal");

        Intent getintent = getIntent();
        categoryid = getintent.getStringExtra("categoryid");
        nutritionid = getintent.getStringExtra("nutritionid");
        creatorid = getintent.getStringExtra("creatorid");

        meal_name = findViewById(R.id.meal_name);
        meal_content = findViewById(R.id.meal_content);
        saveButton = findViewById(R.id.saveButton);
    }

    public void save (View view)
    {

        if (meal_name.getText().toString().equals("") || meal_content.getText().toString().equals(""))
            Toast.makeText(this, "Both Fields Must Not Be Empty!", Toast.LENGTH_SHORT).show();

        else
        {
            saveButton.setEnabled(false);

            String url = "http://batuhanbatu.net/gymkitwebservice/insert_meal.php";

            StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    if (response.contains("Successfully added"))
                    {

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AddMealActivity.this);
                        alertDialogBuilder.setTitle("Added Successfully");
                        alertDialogBuilder.setCancelable(false);
                        alertDialogBuilder.setMessage("The meal was added successfully!");

                        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent intent = new Intent(AddMealActivity.this, NutritionDetailActivity.class);
                                intent.putExtra("nutritionid", nutritionid);
                                intent.putExtra("creatorid", creatorid);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    }

                    else
                    {
                        Toast.makeText(AddMealActivity.this, "Error!", Toast.LENGTH_LONG).show();
                        saveButton.setEnabled(true);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    saveButton.setEnabled(true);
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();

                    params.put("nutritionid", nutritionid);
                    params.put("name", meal_name.getText().toString().replace("'", "~"));
                    params.put("content", meal_content.getText().toString().replace("'", "~"));
                    params.put("categoryid", categoryid);

                    return params;
                }
            };

            Volley.newRequestQueue(this).add(request);
        }
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(AddMealActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(AddMealActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(AddMealActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(AddMealActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(AddMealActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}