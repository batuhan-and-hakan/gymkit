package com.gymkit.gymkit.signedoperations.workout.exercisedetail;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MuscleTabActivity extends AppCompatActivity {

    SharedPreferences session;
    String id = "";
    String name = "";

    TextView exercise_name;
    ImageView musclePhoto1, musclePhoto2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_muscle_tab);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Exercise Details");

        Intent getintent = getIntent();
        id = getintent.getStringExtra("id");
        name = getintent.getStringExtra("exercisename");

        exercise_name = findViewById(R.id.exercise_name);
        musclePhoto1 = findViewById(R.id.musclePhoto1);
        musclePhoto2 = findViewById(R.id.musclePhoto2);

        exercise_name.setText(name);

        getExerciseDetails();

        musclePhoto1.setOnTouchListener(new OnSwipeTouchListener(MuscleTabActivity.this) {
            public void onSwipeTop() {
            }
            public void onSwipeRight() {

                Intent intent = new Intent(MuscleTabActivity.this, ExerciseDetailActivity.class);
                intent.putExtra("id", id);
                intent.putExtra("exercisename", exercise_name.getText().toString());
                startActivity(intent);
                finish();

            }
            public void onSwipeLeft() {

                Intent intent = new Intent(MuscleTabActivity.this, HowTabActivity.class);
                intent.putExtra("id", id);
                intent.putExtra("exercisename", exercise_name.getText().toString());
                startActivity(intent);
                finish();

            }
            public void onSwipeBottom() {
            }

        });

        musclePhoto2.setOnTouchListener(new OnSwipeTouchListener(MuscleTabActivity.this) {
            public void onSwipeTop() {
            }
            public void onSwipeRight() {

                Intent intent = new Intent(MuscleTabActivity.this, ExerciseDetailActivity.class);
                intent.putExtra("id", id);
                intent.putExtra("exercisename", exercise_name.getText().toString());
                startActivity(intent);
                finish();

            }
            public void onSwipeLeft() {

                Intent intent = new Intent(MuscleTabActivity.this, HowTabActivity.class);
                intent.putExtra("id", id);
                intent.putExtra("exercisename", exercise_name.getText().toString());
                startActivity(intent);
                finish();

            }
            public void onSwipeBottom() {
            }

        });
    }

    public void getExerciseDetails()
    {
        String url = "http://batuhanbatu.net/gymkitwebservice/get_exercise_by_id.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("exercise");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        Picasso.get().load(row.getString("photo1")).into(musclePhoto1);
                        Picasso.get().load(row.getString("photo2")).into(musclePhoto2);
                    }

                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("id", id);

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }


    public void toExerciseDetail(View view)
    {
        Intent intent = new Intent(MuscleTabActivity.this, ExerciseDetailActivity.class);
        intent.putExtra("id", id);
        intent.putExtra("exercisename", exercise_name.getText().toString());
        startActivity(intent);
        finish();
    }

    public void toMuscleTab(View view)
    {
        /*Intent intent = new Intent(MuscleTabActivity.this, MuscleTabActivity.class);
        intent.putExtra("id", id);
        startActivity(intent);
        finish();*/
    }

    public void toHowTab(View view)
    {
        Intent intent = new Intent(MuscleTabActivity.this, HowTabActivity.class);
        intent.putExtra("id", id);
        intent.putExtra("exercisename", exercise_name.getText().toString());
        startActivity(intent);
        finish();
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(MuscleTabActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(MuscleTabActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(MuscleTabActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(MuscleTabActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(MuscleTabActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}