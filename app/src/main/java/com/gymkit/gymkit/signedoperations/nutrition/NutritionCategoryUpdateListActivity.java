package com.gymkit.gymkit.signedoperations.nutrition;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;
import com.gymkit.gymkit.signedoperations.workout.addexercise.WorkoutCategoryListActivity;
import com.gymkit.gymkit.signedoperations.workout.addexercise.WorkoutCategoryListRecyclerAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NutritionCategoryUpdateListActivity extends AppCompatActivity {

    SharedPreferences session;
    NutritionCategoryUpdateListRecyclerAdapter nutritionCategoryUpdateListRecyclerAdapter;
    ArrayList<String> idListFromDB;
    ArrayList<String> nameListFromDB;

    String mealid = "";
    String nutritionid = "";
    String creatorid = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nutrition_category_update_list);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Category List");

        Intent getintent = getIntent();
        mealid = getintent.getStringExtra("mealid");
        nutritionid = getintent.getStringExtra("nutritionid");
        creatorid = getintent.getStringExtra("creatorid");

        idListFromDB = new ArrayList<>();
        nameListFromDB = new ArrayList<>();

        getCategoryList();

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        nutritionCategoryUpdateListRecyclerAdapter = new NutritionCategoryUpdateListRecyclerAdapter(this, mealid, nutritionid, creatorid, idListFromDB, nameListFromDB);
        recyclerView.setAdapter(nutritionCategoryUpdateListRecyclerAdapter);
    }

    public void getCategoryList()
    {
        idListFromDB.removeAll(idListFromDB);
        nameListFromDB.removeAll(nameListFromDB);

        String url = "http://batuhanbatu.net/gymkitwebservice/get_nutrition_category_by_nutritionid.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("category");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        idListFromDB.add(String.valueOf(row.getInt("id")));
                        nameListFromDB.add(row.getString("name").replace("~", "'"));

                    }

                    nutritionCategoryUpdateListRecyclerAdapter.notifyDataSetChanged();

                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(NutritionCategoryUpdateListActivity.this, "Error!", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("nutritionid", nutritionid);

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(NutritionCategoryUpdateListActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(NutritionCategoryUpdateListActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(NutritionCategoryUpdateListActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(NutritionCategoryUpdateListActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(NutritionCategoryUpdateListActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}