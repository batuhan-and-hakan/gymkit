package com.gymkit.gymkit.signedoperations.workout.exercisedetail;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.article.ArticleListActivity;
import com.gymkit.gymkit.signedoperations.bodysize.BodySizeListActivity;
import com.gymkit.gymkit.signedoperations.nutrition.NutritionListActivity;
import com.gymkit.gymkit.signedoperations.profile.ProfileActivity;
import com.gymkit.gymkit.signedoperations.workout.WorkoutListActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class HowTabActivity extends AppCompatActivity {

    SharedPreferences session;
    String id = "";
    String name = "";

    TextView exercise_name, howText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_tab);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Exercise Details");

        Intent getintent = getIntent();
        id = getintent.getStringExtra("id");
        name = getintent.getStringExtra("exercisename");

        exercise_name = findViewById(R.id.exercise_name);
        howText = findViewById(R.id.howText);

        exercise_name.setText(name);

        getExerciseDetails();

        howText.setOnTouchListener(new OnSwipeTouchListener(HowTabActivity.this) {
            public void onSwipeTop() {
            }
            public void onSwipeRight() {

                Intent intent = new Intent(HowTabActivity.this, MuscleTabActivity.class);
                intent.putExtra("id", id);
                intent.putExtra("exercisename", exercise_name.getText().toString());
                startActivity(intent);
                finish();

            }
            public void onSwipeLeft() {

            }
            public void onSwipeBottom() {
            }

        });
    }

    public void getExerciseDetails()
    {
        String url = "http://batuhanbatu.net/gymkitwebservice/get_exercise_by_id.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("exercise");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        howText.setText(row.getString("how").replace("~", "'"));
                    }

                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("id", id);

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }


    public void toExerciseDetail(View view)
    {
        Intent intent = new Intent(HowTabActivity.this, ExerciseDetailActivity.class);
        intent.putExtra("id", id);
        intent.putExtra("exercisename", exercise_name.getText().toString());
        startActivity(intent);
        finish();
    }

    public void toMuscleTab(View view)
    {
        Intent intent = new Intent(HowTabActivity.this, MuscleTabActivity.class);
        intent.putExtra("id", id);
        intent.putExtra("exercisename", exercise_name.getText().toString());
        startActivity(intent);
        finish();
    }

    public void toHowTab(View view)
    {
        /*Intent intent = new Intent(HowTabActivity.this, HowTabActivity.class);
        intent.putExtra("id", id);
        startActivity(intent);
        finish();*/
    }

    public void openArticleList(View view)
    {
        Intent intent = new Intent(HowTabActivity.this, ArticleListActivity.class);
        startActivity(intent);
    }

    public void openWorkoutList(View view)
    {
        Intent intent = new Intent(HowTabActivity.this, WorkoutListActivity.class);
        startActivity(intent);
    }

    public void openNutritionList(View view)
    {
        Intent intent = new Intent(HowTabActivity.this, NutritionListActivity.class);
        startActivity(intent);
    }

    public void openBodySizeList(View view)
    {
        Intent intent = new Intent(HowTabActivity.this, BodySizeListActivity.class);
        startActivity(intent);
    }

    public void openProfile(View view)
    {
        Intent intent = new Intent(HowTabActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}