package com.gymkit.gymkit.signedoperations.nutrition;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gymkit.gymkit.R;
import com.gymkit.gymkit.signedoperations.workout.UsersWorkoutListActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UserListToNutritionRecyclerAdapter extends RecyclerView.Adapter<com.gymkit.gymkit.signedoperations.nutrition.UserListToNutritionRecyclerAdapter.PostHolder> {

    private Context context;
    private ArrayList<String> idList;
    private ArrayList<String> fnameList;
    private ArrayList<String> lnameList;


    public UserListToNutritionRecyclerAdapter(Context context, ArrayList<String> idList, ArrayList<String> fnameList, ArrayList<String> lnameList) {
        this.context = context;
        this.idList = idList;
        this.fnameList = fnameList;
        this.lnameList = lnameList;
    }

    @NonNull
    @Override
    public com.gymkit.gymkit.signedoperations.nutrition.UserListToNutritionRecyclerAdapter.PostHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.user_list_recycler_row,parent,false);
        return new com.gymkit.gymkit.signedoperations.nutrition.UserListToNutritionRecyclerAdapter.PostHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final com.gymkit.gymkit.signedoperations.nutrition.UserListToNutritionRecyclerAdapter.PostHolder holder, final int position) {

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, UsersNutritionListActivity.class);
                intent.putExtra("userid", idList.get(position));
                context.startActivity(intent);

            }
        });

        holder.nameTextView.setText(fnameList.get(position) + " " + lnameList.get(position));

    }

    @Override
    public int getItemCount() {
        return idList.size();
    }

    class PostHolder extends RecyclerView.ViewHolder {

        ConstraintLayout container;
        TextView nameTextView;

        public PostHolder(@NonNull final View itemView) {
            super(itemView);

            container = itemView.findViewById(R.id.user_list_container);
            nameTextView = itemView.findViewById(R.id.nameRecycler);

        }
    }
}
